package cz.kor3k.snake.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cz.kor3k.forge.Hostname;
import cz.kor3k.snake.SnakeGame;

public class DesktopLauncher {
	public static void main (String[] arg)
    {
        LwjglApplicationConfiguration config        =       createConfig();

        new LwjglApplication(new SnakeGame( new Hostname.DesktopHostname() ), config);
	}

    protected static LwjglApplicationConfiguration createConfig()
    {
            LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

            config.title            =   "Snake";
            config.fullscreen       =   false;
            config.foregroundFPS    =   60;
            config.width            =   800;
            config.height           =   600;
            config.allowSoftwareMode=   true;

            config.addIcon( "icon_lady_128.png" , Files.FileType.Internal );
            config.addIcon( "icon_lady_32.png" , Files.FileType.Internal );
            config.addIcon( "icon_lady_16.png" , Files.FileType.Internal );

            return config;
    }
}
