package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import java.io.IOException;
import java.lang.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 12. 12. 2014.
 */
public class KryoNet
{
    public static final List<Client> clients =   new ArrayList<>();
    public static final List<Server> servers =   new ArrayList<>();

    static
    {
        java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
        java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
    }

    private static void log( String tag , String message )
    {
        try
        {
            Gdx.app.log(tag, message);
        }
        catch ( NullPointerException e )
        {
            System.out.println( tag + " " + message );
        }
    }

    public static class Client extends com.esotericsoftware.kryonet.Listener
    {
        protected com.esotericsoftware.kryonet.Client kryoClient;
        protected Connection connection;
        protected URI url;
        protected int     timeout;
        protected int     tcpPort;
        protected int     udpPort;
        protected String  host;

        public Client(int timeout, String host, int tcpPort, int udpPort) throws IOException
        {
            this.timeout    =   timeout;
            this.host       =   host;
            this.tcpPort    =   tcpPort;
            this.udpPort    =   udpPort;
            this.kryoClient =   this.createKryoClient();

            try
            {
                this.url =   new URI( "/" + host + ":" + tcpPort );
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }

            KryoNet.clients.add( this );
        }

        protected com.esotericsoftware.kryonet.Client createKryoClient( int writeBufferSize , int objectBufferSize )
        {
            com.esotericsoftware.kryonet.Client client  =   new com.esotericsoftware.kryonet.Client( writeBufferSize , objectBufferSize );

            client.addListener( this );
            client.getKryo().register( Net.Message.class );
            client.getKryo().register( Net.Message.MessageType.class );

            return client;
        }

        protected com.esotericsoftware.kryonet.Client createKryoClient()
        {
            return createKryoClient( 8192, 2048 );
        }

        public Thread create()
        {
            Thread thread   =   new Thread( kryoClient );
            thread.start();

            kryoClient.addListener( this );
            kryoClient.getKryo().register(Net.Message.class);
            kryoClient.getKryo().register(Net.Message.MessageType.class);

            try
            {
                kryoClient.connect( timeout , host , tcpPort , udpPort );
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return thread;
        }

        public void dispose()
        {
            try
            {
                kryoClient.dispose();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            KryoNet.clients.remove(this);
        }

        public URI getUrl()
        {
            return url;
        }

        public void connected(Connection connection)
        {
            this.connection    =   connection;

            log("KryoClient", "session connected");
        }

        public void disconnected(Connection connection)
        {
            this.connection    =   null;

            log("KryoClient", "session disconnected");
        }

        public void received(Connection conn, java.lang.Object object)
        {
//            log("KryoClient", object.toString());
        }

        public void idle(Connection connection)
        {
//        log( "KryoClient" , "session idle" );
        }

        public void send(String message)
        {
            kryoClient.sendTCP(message);
        }

        public void send(Net.Message message)
        {
            kryoClient.sendTCP(message);
//            this.send(Net.Message.toJson(message));
        }

        public void send(Object message)
        {
            kryoClient.sendTCP(message);
        }
    }

    public static class Server extends com.esotericsoftware.kryonet.Listener
    {
        protected com.esotericsoftware.kryonet.Server kryoServer;
        protected List<Connection>    connections     =   new ArrayList<>();
        protected URI                 url;
        protected int     tcpPort;
        protected int     udpPort;
        protected String  host;

        public Server( String hostname , int tcpPort , int udpPort ) throws IOException
        {
            this.host       =   hostname;
            this.tcpPort    =   tcpPort;
            this.udpPort    =   udpPort;
            this.kryoServer =   this.createKryoServer();

            try
            {
                url =   new URI( "/" + host + ":" + tcpPort );
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
            }

            KryoNet.servers.add( this );
        }

        public Thread create()
        {
            Thread thread   =   new Thread( kryoServer );
            thread.start();

            try
            {
                kryoServer.bind( new InetSocketAddress( host , tcpPort ) , new InetSocketAddress( host , udpPort ) );
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return thread;
        }

        protected com.esotericsoftware.kryonet.Server createKryoServer( int writeBufferSize , int objectBufferSize )
        {
            com.esotericsoftware.kryonet.Server server  =   new com.esotericsoftware.kryonet.Server( writeBufferSize , objectBufferSize );

            server.addListener( this );
            server.getKryo().register( Net.Message.class );
            server.getKryo().register( Net.Message.MessageType.class );

            return server;
        }

        protected com.esotericsoftware.kryonet.Server createKryoServer()
        {
            return createKryoServer( 16384, 2048 );
        }

        public void disconnected(Connection connection)
        {
            connections.remove( connection );

            log("KryoServer", "session disconnected");
        }

        public void connected(Connection connection)
        {
            if( !connections.contains( connection ) )
                connections.add( connection );

            log("KryoServer", "session connected");
        }

        public void received(Connection conn, java.lang.Object object)
        {
            log("KryoServer", object.toString());
        }

        public void idle(Connection connection)
        {
//        log( "KryoServer" , "session idle" );
        }

        public void broadcast(String message)
        {
//            log("KryoServer", "broadcasting " + message);
            kryoServer.sendToAllUDP(message);
            //TODO: use TcpIdleSender + abstract queue/idle sender?
            //TODO: increase buffer size OR use only tcp
            //TODO: use a bound service instead a thread for server and client
        }

        public void broadcast(cz.kor3k.snake.Net.Message message)
        {
            kryoServer.sendToAllUDP( message );
//        this.broadcast( Net.Message.toJson( message ) );
        }

        public void broadcast( Object message )
        {
            kryoServer.sendToAllUDP( message );
        }

        public void dispose()
        {
            try
            {
                kryoServer.dispose();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            KryoNet.servers.remove(this);
        }

        public URI getUrl()
        {
            return url;
        }

        public List<Connection> getConnections()
        {
            return this.connections;
        }

    }
}
