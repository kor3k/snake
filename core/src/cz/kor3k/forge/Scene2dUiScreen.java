package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by martin on 30. 11. 2014.
 */
public class Scene2dUiScreen extends Screen
{
    protected Stage       stage;
    protected Skin        skin;
    protected Table       container;
    protected ScrollPane  scrollPane;
    protected Table       table;

    public Scene2dUiScreen(Game game)
    {
        super(game);
    }

    @Override
    public Screen create()
    {
        this.prepareUi( null );
        container.add( scrollPane ).expand().fill();
        container.row().space(10).padBottom(10);

        return this;
    }

    protected void prepareUi( Skin uiskin )
    {
        skin        =   uiskin;
        stage       =   new Stage( new ScreenViewport() );
        container   =   new Table( skin );
        table       =   new Table( skin );
        scrollPane  =   new ScrollPane( table , skin );

        Gdx.input.setInputProcessor( stage );
        scrollPane.setScrollingDisabled(true, false);
        stage.addActor( container );

        table.defaults().space( 10 );
    }

    protected void clear()
    {
        container.clear();
        table.clear();
    }

    @Override
    public void render ( float delta )
    {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act( Gdx.graphics.getDeltaTime() );
        stage.draw();
    }

    @Override
    public void resize (int width, int height)
    {
        stage.getViewport().update(width, height, true);
        container.setWidth( width );
        container.setHeight( height );
    }

    protected Label createLabel( String text , Color color , BitmapFont font )
    {
        Label.LabelStyle style  =   new Label.LabelStyle( skin.get( Label.LabelStyle.class ) );
        style.fontColor         =   color;
        style.font              =   font;
        Label label             =   new Label ( text , style );

        return label;
    }

    protected Label createLabel( String text , Color color )
    {
        Label label = new Label ( text , skin.get( Label.LabelStyle.class ) );
        label.setColor(color);

        return label;
    }

    protected TextButton createBackButton( Screen screen )
    {
        return createButton( "back" , screen );
    }

    protected TextButton createButton( String text , final Screen screen )
    {
        TextButton button = new TextButton( text , skin );
        button.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        System.out.println("click " + x + ", " + y);
                        game.setScreen( screen );

                        if( screen instanceof Scene2dUiScreen )
                        {
                            screen.create();
                        }
                    }
                });

        return button;
    }

    @Override
    public void dispose ()
    {
        stage.dispose();
    }
}
