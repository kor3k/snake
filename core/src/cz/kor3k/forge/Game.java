package cz.kor3k.forge;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public abstract class Game extends com.badlogic.gdx.Game
{

    public SpriteBatch          batch;
    public BitmapFont           font;
    public ShapeRenderer        shape;
    public OrthographicCamera   camera;
    protected int               screenWidth;
    protected int               screenHeight;

    public static Game obj;

    public Game()
    {
        obj    =   this;
    }

    public enum State
    {
        READY,
        RUNNING,
        PAUSED,
        OVER
    }

    public int getScreenWidth()
    {
        return this.screenWidth;
    }

    public int getScreenHeight()
    {
        return this.screenHeight;
    }

    public void create()
    {
        this.batch   =  new SpriteBatch();
        this.font    =  new BitmapFont();
        this.camera  =  new OrthographicCamera();
        this.shape   =  new ShapeRenderer();
        Gdx.app.setLogLevel( Application.LOG_ERROR );
    }

    public void render()
    {
        super.render(); //important!

        this.camera.update();
        this.batch.setProjectionMatrix( this.camera.combined );
        this.shape.setProjectionMatrix( this.camera.combined );
    }

    public void dispose()
    {
        super.dispose(); //???
        batch.dispose();
        font.dispose();
        shape.dispose();
    }

    public void resize(int width, int height)
    {
        this.screenWidth    =   width;
        this.screenHeight   =   height;
//        this.camera.setToOrtho( false , width * 2 , height * 2 );
        this.camera.setToOrtho( false , width , height );
        super.resize(width, height);
    }

    @Override
    public void setScreen(com.badlogic.gdx.Screen screen)
    {
        com.badlogic.gdx.Screen current = this.getScreen();
        super.setScreen(screen);

        if( null != current )
        {
            current.dispose();
            current = null;
        }
    }

    public void clearScreen( float red , float green , float blue , float alpha )
    {
        Gdx.gl.glClearColor( red , green , blue , alpha );
//        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT ); // This cryptic line clears the view.
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
    }

    public void clearScreen()
    {
        this.clearScreen( 0.0f , 0.0f , 0.0f , 1 );
    }

}