package cz.kor3k.forge;

import java.util.Iterator;
import java.util.List;

/**
 * Created by martin on 13. 11. 2014.
 */
public abstract class AbstractTimeoutable implements Timeoutable
{
    public void subtractTimeout( float time )
    {
        this.setTimeout( this.getTimeout() - time );
    }

    public boolean isTimeouted()
    {
        return this.getTimeout() <= 0;
    }

    public static void subtractTimeout( float time , List<Timeoutable> list )
    {
        for( Timeoutable to : list )
        {
            to.subtractTimeout( time );
        }
    }

    public static void dropTimeouted( List<Timeoutable> list )
    {
        for (Iterator<Timeoutable> it = list.iterator(); it.hasNext();)
        {
            Timeoutable to = it.next();
            if( to.isTimeouted() )
            {
                it.remove();
            }
        }
    }
}
