package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;

/**
 * Created by martin on 29. 10. 2014.
 */
public abstract class World
{
    private Game.State  gameState;

    public void setGameState( Game.State state )
    {
        this.gameState  =   state;
    }

    public Game.State getGameState()
    {
        return this.gameState;
    }

    public void pause()
    {
        if( Game.State.RUNNING == this.getGameState() || Game.State.READY == this.getGameState() )
        {
            Gdx.app.debug("World", "pause()ing");
            this.setGameState( Game.State.PAUSED );
        }
    }

    public void resume()
    {
        if (Game.State.PAUSED == this.getGameState())
        {
            Gdx.app.debug("World", "resume()ing");
            this.setGameState(Game.State.RUNNING);
        }
    }

    public boolean isRunning()
    {
        return Game.State.RUNNING == this.getGameState();
    }

    public boolean isPaused()
    {
        return Game.State.PAUSED == this.getGameState();
    }

    public boolean isReady()
    {
        return Game.State.READY == this.getGameState();
    }

    public boolean isOver()
    {
        return Game.State.OVER == this.getGameState();
    }

    public abstract void update( float deltaTime );
}
