package cz.kor3k.forge;

import java.util.Iterator;
import java.util.List;

public interface Timeoutable
{
    float   getTimeout();

    void    setTimeout( float timeout );

    void    subtractTimeout( float time );
    
    boolean isTimeouted();
}
