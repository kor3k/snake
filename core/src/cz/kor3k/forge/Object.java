package cz.kor3k.forge;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * a 2D in-game object representation
 */
public class Object {
    public final Vector2 position;
    public final Rectangle bounds;

    public Object(float x, float y, float width, float height)
    {
        this.position   =   new Vector2(x, y);
        this.bounds     =   new Rectangle(x, y, width, height);
//        this.bounds = new Rectangle(x - width / 2, y - height / 2, width, height);
    }

    public Object()
    {
        this.position   =   new Vector2();
        this.bounds     =   new Rectangle();
    }

    public boolean overlaps( Object anotherObject )
    {
        return this.bounds.overlaps( anotherObject.bounds );
    }

    public void setPosition( float x , float y )
    {
        this.bounds.setPosition( x , y );
        this.position.set( x , y );
    }

    public void setPosition( Vector2 position )
    {
        this.setPosition( position.x , position.y );
    }
}