package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;

import java.net.URI;
import java.util.List;

/**
 * Created by martin on 7. 12. 2014.
 */
public class Net
{
    public static interface Server extends Disposable
    {
        void broadcast( String message );
        void broadcast( Message message );
        URI getUrl();
        List getConnections();
        Thread create();
    }

    public static interface WorldServer extends Server
    {
        void broadcast( World world );
    }

    public static interface Client extends Disposable
    {
        void send( String message );
        void send( Message message );
        URI getUrl();
        Thread create();
    }

    public static interface Factory
    {
        Client createClient( String host , String port );
        Server createServer( String host , String port );
    }

    public static class Message
    {
        private java.lang.Object payload;
        private MessageType type;

        public Message( MessageType type , java.lang.Object payload )
        {
            this.setType( type );
            this.setPayload( payload );
        }

        public Message()
        {}

        public static String toJson( Message message )
        {
            return new Json().toJson( message );
        }

        public static Message fromJson( String json )
        {
            return new Json().fromJson( Message.class , json );
        }

        public java.lang.Object getPayload()
        {
            return payload;
        }

        public void setPayload(java.lang.Object payload)
        {
            this.payload = payload;
        }

        public MessageType getType()
        {
            return type;
        }

        public void setType(MessageType type)
        {
            this.type = type;
        }

        public static enum MessageType
        {
            CONNECT, WORLD, CONTROLLER, CHAT, START
        }
    }

    /**
     *
     * 0    -   protocol ( ws:// | wss:// )
     * 1    -   host
     * 2    -   port
     * 3    -   path (or empty string)
     *
     * @param uri
     * @return String[]
     */
    public static String[] parseUrl( String uri )
    {
//      ws://localhost:8025/ws;
        String[]    parsed  =   new String[4];

        Gdx.app.debug("uri", uri);

        if( uri.contains( "wss://" ) )
        {
            parsed[0]   =   uri.substring( 0 , 6 );
            uri         =   uri.substring( 6 );
        }
        else if( uri.contains( "ws://" ) )
        {
            parsed[0]   =   uri.substring( 0 , 5 );
            uri         =   uri.substring( 5 );
        }
        else if( uri.contains( "http://" ) )
        {
            parsed[0]   =   uri.substring( 0 , 7 );
            uri         =   uri.substring( 7 );
        }
        else if( uri.contains( "https://" ) )
        {
            parsed[0]   =   uri.substring( 0 , 8 );
            uri         =   uri.substring( 8 );
        }
        else
        {
            parsed[0]   =   "ws://";
        }
        Gdx.app.debug( "protocol" , parsed[0] );

        boolean hasPath =   uri.substring( uri.indexOf(":") ).contains( "/" );
        Gdx.app.debug( "hasPath" , hasPath ? "true" : "false" );

        parsed[1]   =   uri.substring( 0 , uri.indexOf( ":" ) );
        Gdx.app.debug( "host" , parsed[1] );

        parsed[2]   =   uri.substring( uri.indexOf( ":" )+1 ,  hasPath ? uri.indexOf( "/" ) : uri.length() );
        Gdx.app.debug( "port" , parsed[2] );

        if( hasPath )
        {
            parsed[3]    =   uri.substring( uri.indexOf( "/" ) );
        }
        else
        {
            parsed[3]    =  "";
        }
        Gdx.app.debug( "path" , parsed[3] );

        Gdx.app.debug( "Parsed uri: " , parsed[0]+parsed[1]+parsed[2]+parsed[3] );

        return parsed;
    }
}
