package cz.kor3k.forge;

import com.badlogic.gdx.math.Shape2D;
import java.io.Serializable;

/**
 * Created by martin on 6. 11. 2014.
 */
public class Triangle implements Serializable, Shape2D
{
    public float x1, y1, x2, y2, x3, y3;

    public Triangle( float x1 , float y1 , float x2 , float y2 , float x3 , float y3 )
    {
        this.x1 =   x1;
        this.y1 =   y1;
        this.x2 =   x2;
        this.y2 =   y2;
        this.x3 =   x3;
        this.y3 =   y3;
    }
}
