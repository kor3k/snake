package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.*;

/**
 * Created by martin on 28. 10. 2014.
 */
public abstract class Screen implements com.badlogic.gdx.Screen
{
    public  Game game;

    public Screen( final Game game )
    {
        this.game   =   game;
    }

    protected boolean inBounds( Vector3 touchPos, float x, float y, float width, float height)
    {
        Gdx.app.debug("forge.Screen", "starting calculate inBounds()");
        if (touchPos.x > x && touchPos.x < x + width -1 &&
                touchPos.y > y && touchPos.y < y + height -1){
            Gdx.app.debug("forge.Screen", "ended calculate inBounds() with true result");
            return true;
        } else{
            Gdx.app.debug("forge.Screen", "ended calculate inBounds() with false result");
            return false;
        }
    }

    public Screen create()
    {
        Gdx.app.debug("forge.GameScreen", "create()ing");
        return this;
    }

    public void render (float delta)
    {
        Gdx.app.debug("forge.Screen", "render()ing with delta: " + Float.toString( delta ) );
    }

    public void resize (int width, int height)
    {
        Gdx.app.debug("forge.Screen", "resize()ing to: " + Integer.toString( width ) + "x" + Integer.toString( height ) );
    }

    public void show ()
    {
        Gdx.app.debug("forge.Screen", "show()ing");
    }

    public void hide ()
    {
        Gdx.app.debug("forge.Screen", "hide()ing");
    }

    public void pause ()
    {
        Gdx.app.debug("forge.Screen", "pause()ing");
    }

    public void resume ()
    {
        Gdx.app.debug("forge.Screen", "resume()ing");
    }

    public void dispose ()
    {
        Gdx.app.debug("forge.Screen", "dispose()ing");
    }

    protected void draw( CharSequence text , Color color , Vector2 position )
    {
        game.font.setColor( color );
        game.font.draw( game.batch, text , position.x , position.y );
    }

    protected void draw( CharSequence text , Color color , Vector2 position , BitmapFont font )
    {
        font.setColor( color );
        font.draw( game.batch, text , position.x , position.y );
    }

    protected void draw( Texture texture , Rectangle position )
    {
        game.batch.draw( texture , position.x , position.y , position.width , position.height );
    }

    protected void draw( Rectangle rectangle , Color color )
    {
        game.shape.setColor( color );
        game.shape.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    protected void draw( Circle circle , Color color )
    {
        game.shape.setColor( color );
        game.shape.circle( circle.x , circle.y , circle.radius );
    }

    protected void draw( Ellipse ellipse , Color color )
    {
        game.shape.setColor( color );
        game.shape.ellipse( ellipse.x , ellipse.y , ellipse.width , ellipse.height );
    }

    protected void draw( Triangle triangle , Color color )
    {
        game.shape.setColor( color );
        game.shape.triangle( triangle.x1 , triangle.y1 , triangle.x2 , triangle.y2 , triangle.x3 , triangle.y3 );
    }
}
