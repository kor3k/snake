package cz.kor3k.forge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 2. 11. 2014.
 */
public class Ticker
{
    private float tickTime  =   0;
    private float tick      =   0;

    public Ticker( float tick )
    {
        this.tick   =   tick;
    }

    public float getTick()
    {
        return tick;
    }

    public float getTickTime()
    {
        return tickTime;
    }

    public void setTick( float tick )
    {
        this.tick   =   tick;
    }

    public void tick()
    {
        tickTime -= tick;
    }

    public void addDeltaTime( float deltaTime )
    {
        tickTime += deltaTime;
    }

    public boolean isTickQueued()
    {
        return tickTime > tick;
    }

    public static class TickerMultiplexer
    {
        private List<Ticker> tickers =   new ArrayList<>();

        public TickerMultiplexer addTicker( Ticker ticker )
        {
            this.tickers.add( ticker );
            return this;
        }

        public List<Ticker> getTickers()
        {
            return this.tickers;
        }

        public void addDeltaTime( float deltaTime )
        {
            for( Ticker t : this.tickers )
            {
                t.addDeltaTime( deltaTime );
            }
        }

        public void tick()
        {
            for( Ticker t : this.tickers )
            {
                t.tick();
            }
        }

        public boolean isTickQueued()
        {
            for( Ticker t : this.tickers )
            {
                if( t.isTickQueued() )
                    return true;
            }

            return false;
        }
    }
}
