package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by martin on 29. 10. 2014.
 */
public class Assets extends AssetManager
{
    public static void playSound (Sound sound) {
//        if (Settings.soundEnabled) sound.play(1);
        if (true) sound.play(1);
    }

    public Assets create()
    {
        Gdx.app.debug( "Assets" , "starting loading assets" );
        //místo tohohle Runnable, new Thread ?
        this.loadAssets();
        this.finishLoading();
        Gdx.app.debug( "Assets" , "ended loading assets" );
        return this;
    }

    public BitmapFont createFont( int size , FileHandle ttfFile )
    {
        FreeTypeFontGenerator.FreeTypeFontParameter params      =   new FreeTypeFontGenerator.FreeTypeFontParameter();
        params.size =    size;
        return createFont( params , ttfFile );
    }

    public BitmapFont createFont( FreeTypeFontGenerator.FreeTypeFontParameter params , FileHandle ttfFile )
    {
        FreeTypeFontGenerator   generator   =   new FreeTypeFontGenerator( ttfFile );
        BitmapFont              font        =   generator.generateFont( params );

        generator.dispose();
        return font;
    }

    @Override
    public synchronized <T> T get(String fileName, Class<T> type)
    {
        if( !this.isLoaded( fileName , type ) )
        {
            this.load( fileName , type );
            this.finishLoading();
        }

        return super.get( fileName, type );
    }

    protected void loadAssets()
    {
        //this.load( "joint.png" , Texture.class );
        //this.load( "bell.wav" , Sound.class );
    }
}
