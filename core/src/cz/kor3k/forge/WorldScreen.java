package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;

/**
 * Created by martin on 29. 10. 2014.
 */
public abstract class WorldScreen extends Screen
{
    public WorldScreen(Game game)
    {
        super(game);
    }

    protected void renderView( float delta )
    {
        Gdx.app.debug("forge.GameScreen", "renderView() with delta: " + Float.toString( delta ) );
    }

    protected void updateModel( float delta )
    {
        Gdx.app.debug("forge.GameScreen", "updateModel() with delta: " + Float.toString( delta ) );
    }

    public void render( float delta )
    {
        this.updateModel(delta);
        this.renderView(delta);
    }
}
