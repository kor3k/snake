package cz.kor3k.forge;

import com.badlogic.gdx.Gdx;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_76;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.handshake.ServerHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.lang.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.channels.NotYetConnectedException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 1. 12. 2014.
 */
public class WebSockets
{
    public static final List<Client> clients =   new ArrayList<>();
    public static final List<Server> servers =   new ArrayList<>();

    static
    {
        java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
        java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
    }

    private static void log( String tag , String message )
    {
        try
        {
            Gdx.app.log( tag , message );
        }
        catch ( NullPointerException e )
        {
            System.out.println( tag + " " + message );
        }
    }

    public static class Client extends WebSocketClient
    {
        public Client(URI serverURI)
        {
            this(serverURI, new Draft_76()); //TODO: maybe, just maybe, the lagging on android can be caused by the draft? try different ones?
        }

        public Client(URI serverUri, Draft draft)
        {
            super(serverUri, draft);
            WebSockets.clients.add( this );
        }

        public Client( String hostname, int port ) throws URISyntaxException
        {
//            this( new URI( "/" + hostname + ":" + port ) );
            this( new URI(
                    0 == hostname.indexOf( "wss://" ) ? hostname : ( 0 == hostname.indexOf( "ws://" ) ? hostname : "ws://" + hostname )
                            + ":" + port ) );
        }

        public Thread create()
        {
            Thread  thread =   new Thread( this );
            thread.start();
            return thread;
        }

        public URI getUrl()
        {
            return getURI();
        }

        @Override
        public void onOpen(ServerHandshake handshakedata)
        {
            log( "WebSockets.Client onOpen" , null == handshakedata ? "" : handshakedata.toString() );
        }

        @Override
        public void onMessage(String message)
        {
            log( "WebSockets.Client onMessage" , message );
        }

        @Override
        public void onClose(int code, String reason, boolean remote)
        {
            log( "WebSockets.Client onClose" , code + " " + reason + " " + remote );
        }

        @Override
        public void onError(Exception ex)
        {
            ex.printStackTrace();
            log( "WebSockets.Client onError" , ex.toString() );
        }

        @Override
        public void send(String text) throws NotYetConnectedException
        {
            super.send(text);
        }

        public void send( Net.Message message )
        {
            this.send( Net.Message.toJson( message ) );
        }

        public void dispose()
        {
            this.close();
            WebSockets.clients.remove( this );
        }
    }

    public static class Server extends WebSocketServer
    {
        protected List<WebSocket> connections =   new ArrayList<>();

        public Server(InetSocketAddress address)
        {
            super(address);
            WebSockets.servers.add( this );
        }

        public Server( String hostname , int port ) throws UnknownHostException
        {
            super(new InetSocketAddress(hostname, port));
            WebSockets.servers.add( this );
        }

        public Thread create()
        {
            Thread  thread =   new Thread( this );
            thread.start();
            return thread;
        }

        public URI getUrl()
        {
            try {
                return new URI( this.getAddress().toString() );
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected boolean addConnection(WebSocket ws)
        {
            synchronized ( connections )
            {
                this.connections.add(ws);
            }

            log( "WebSockets.Server.addConnection" , String.valueOf(this.connections.size()) );

            return super.addConnection(ws);
        }

        @Override
        protected boolean removeConnection(WebSocket ws)
        {
            synchronized ( connections )
            {
                this.connections.remove(ws);
            }

            log( "WebSockets.Server" , "removeConnection" );
            log( "WebSockets.Server" , ws.toString() );
            log( "WebSockets.Server" , String.valueOf( this.connections.size() ) );

            return super.removeConnection(ws);
        }

        @Override
        public List<WebSocket> connections()
        {
            return this.connections;
        }

        public List<WebSocket> getConnections()
        {
            return this.connections;
        }

        @Override
        public void onOpen(WebSocket conn, ClientHandshake handshake)
        {
            log("WebSockets.Server onOpen", conn.toString() + " " + handshake.toString());
        }

        @Override
        public void onClose(WebSocket conn, int code, String reason, boolean remote)
        {
            log("WebSockets.Server onClose", conn.toString() + " " + code + " " + reason + " " + remote);
        }

        @Override
        public void onMessage(WebSocket conn, String message)
        {
            log("WebSockets.Server onMessage", conn.toString() + " " + message);
        }

        @Override
        public void onError(WebSocket conn, Exception ex)
        {
            log("WebSockets.Server onError", null == conn ? "no connection" : conn.toString() + " " + conn.getRemoteSocketAddress() );
            ex.printStackTrace();
        }

        /**
         * Sends <var>text</var> to all currently connected WebSocket clients.
         *
         * @param text
         *            The String to send across the network.
         * @throws InterruptedException
         *             When socket related I/O errors occur.
         */
        public void broadcast( String text )
        {
//            Collection<WebSocket> con = connections();
            List<WebSocket> con = this.connections();
            synchronized ( con )
            {
                for( WebSocket c : con )
                {
                    c.send( text );
                }
            }
        }

        public void broadcast( Net.Message message )
        {
            this.broadcast( Net.Message.toJson( message ) );
        }

        public void dispose()
        {
            try
            {
                this.stop();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            WebSockets.servers.remove( this );
        }

        public void drop( WebSocket conn )
        {
            this.removeConnection( conn );
            conn.close( CloseFrame.GOING_AWAY );
        }

        public void drop( int conn )
        {
        }

        public void dropAll()
        {
            for( WebSocket conn : this.connections() )
            {
                this.drop( conn );
            }
        }
    }
}
