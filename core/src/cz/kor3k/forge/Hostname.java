package cz.kor3k.forge;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * Created by martin on 3. 12. 2014.
 */
public interface Hostname
{
        String getHostname();

        public static class DesktopHostname implements Hostname
        {

                @Override
                public String getHostname()
                {
                        InetAddress thisIp;
                        try
                        {
                                thisIp = InetAddress.getLocalHost();

//            return InetAddress.getLocalHost().getHostAddress();
                                return InetAddress.getLocalHost().getCanonicalHostName();
                        }
                        catch (UnknownHostException e)
                        {
                                e.printStackTrace();
                                return ("127.0.0.1");
                        }
                }
        }

        public static class AndroidHostname implements Hostname
        {

                @Override
                public String getHostname()
                {
                        //This code works only for android devices
                        //Thanks to: http://www.droidnova.com/get-the-ip-address-of-your-device,304.html
                        try
                        {
                                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
                                {
                                        NetworkInterface intf = en.nextElement();
                                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
                                        {
                                                InetAddress inetAddress = enumIpAddr.nextElement();
                                                if (!inetAddress.isLoopbackAddress())
                                                {
//                        return inetAddress.getHostAddress();
                                                        return inetAddress.getCanonicalHostName();
                                                }
                                        }
                                }
                        }
                        catch (SocketException ex)
                        {
                                System.out.println("Not an android!!!");
                                return "";
                        }

                        return ("127.0.0.1");
                }
        }
}
