package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.SnakeGame;

/**
 * Created by martin on 23. 11. 2014.
 */
public class TestScreen1 extends Screen
{
    Stage   stage;
    Skin    skin;
    Table   container;

    public TestScreen1(Game game)
    {
        super(game);
        this.create();
    }

    public Screen create ()
    {
        stage       =   new Stage( new ScreenViewport() );
        container   =   new Table();
        skin        =   new Skin( Gdx.files.internal( "skin/uiskin.json" ) );

        Gdx.input.setInputProcessor(stage);

//        container.setFillParent( true );

        ScrollPane pane2 = new ScrollPane(new Image(new Texture("skin/group-debug.png")), skin);
        pane2.setScrollingDisabled(false, true);
        // pane2.setCancelTouchFocus(false);
        pane2.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                event.stop();
                return true;
            }
        });


//        container.debug();
        container.add(new Image(new Texture("skin/group-debug.png")));
        container.row();
        container.add(new Image(new Texture("skin/group-debug.png")));
        container.row();
        container.add(pane2).size(100);
        container.row();
        container.add(new Image(new Texture("skin/group-debug.png")));
        container.row();
        container.add(new Image(new Texture("skin/group-debug.png")));

        ScrollPane pane = new ScrollPane(container, skin);
        pane.setScrollingDisabled(true, false);
        // pane.setCancelTouchFocus(false);
        if (false) {
            // This sizes the pane to the size of it's contents.
            pane.pack();
            // Then the height is hardcoded, leaving the pane the width of it's contents.
            pane.setHeight(Gdx.graphics.getHeight());
        } else {
            // This shows a hardcoded size.
            pane.setWidth( Gdx.graphics.getWidth() );
            pane.setHeight( Gdx.graphics.getHeight() );
        }

        stage.addActor(pane);

        return this;
    }

    @Override
    public void render ( float delta )
    {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    public void render2( float delta )
    {
        SnakeGame game = (SnakeGame) this.game;

        game.clearScreen(0.0f, 0.0f, 0.2f, 1);

        //initialize WsServer
        //listen for connections. accept user settings (name + id), add users
        //bind network controllers to network users
        //when all connected, send gamestart and set screen to new ServerWorldScreen( WsServer )
    }

    @Override
    public void resize (int width, int height)
    {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose ()
    {
        stage.dispose();
        skin.dispose();
    }
}
