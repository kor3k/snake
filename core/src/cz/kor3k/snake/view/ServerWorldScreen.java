package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import cz.kor3k.forge.Game;
import cz.kor3k.snake.Net;
import cz.kor3k.forge.Screen;
import cz.kor3k.forge.Ticker;
import cz.kor3k.snake.Settings;
import cz.kor3k.snake.controller.KeyboardController;
import cz.kor3k.snake.controller.PlayerController;
import cz.kor3k.snake.controller.TouchController;
import cz.kor3k.snake.model.Player;
import cz.kor3k.snake.model.World;


/**
 * Created by martin on 16. 11. 2014.
 */

public class ServerWorldScreen extends WorldScreen
{
    private Net.Server server;
    private Ticker      ticker;

    public ServerWorldScreen( Net.Server server )
    {
        super();
        this.server =   server;
    }

    @Override
    public Screen create()
    {
        this.world              =   new World( 40 , 30 , game );
        ticker                  =   new Ticker( settings.gameSpeed );

        Gdx.app.log( "ServerWorldScreen" , "main thread name: " + Thread.currentThread().getName() );

        InputMultiplexer input  =   new InputMultiplexer();
        input.addProcessor( new KeyboardController( this.world , this.game ) );
        input.addProcessor( new TouchController( this.world , this.game ) );
        Gdx.input.setInputProcessor( input );

        for( Settings.Player p : settings.getPlayers() )
        {
            Player player =   new Player( p.id );
            PlayerController playerCtrl;

            //TODO: get rid of the netConnection, netKeyboard, netTouch
            if( p.isNet )
            {
                playerCtrl  =   new PlayerController.Network.Server( player , server );
                game.playerControllers.add( playerCtrl );
            }
            else
            {
                if( p.leftKey != null && p.rightKey != null )
                {
                    playerCtrl  =   new PlayerController.Keyboard( player , p.leftKey , p.rightKey );
                    game.playerControllers.add( playerCtrl );
                    input.addProcessor( playerCtrl );
                }

                if( p.leftArea != null && p.rightArea != null )
                {
                    playerCtrl  =   new PlayerController.Touch( player , p.leftArea , p.rightArea );
                    game.playerControllers.add( playerCtrl );
                    input.addProcessor( playerCtrl );
                }
            }

            this.world.players.add(player);
        }

        this.world.create();
        this.world.setGameState( Game.State.READY );
        Gdx.input.setCatchBackKey( true );

        return this;
    }

    @Override
    public void updateModel(float delta)
    {
        super.updateModel(delta);

        ticker.addDeltaTime(delta);

        //TODO: perhaps if instead while (tick once always)
        while( ticker.isTickQueued() )
        {
//            Gdx.app.log("ServerWorldScreen", "sending world");
            ((Net.WorldServer)server).broadcast( world );
            ticker.tick();
        }
    }

    @Override
    public void dispose()
    {
        server.dispose();
        super.dispose();
    }
}
