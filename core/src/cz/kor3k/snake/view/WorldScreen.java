package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import cz.kor3k.forge.Screen;
import cz.kor3k.forge.Triangle;
import cz.kor3k.snake.Settings;
import cz.kor3k.snake.SnakeGame;
import cz.kor3k.snake.controller.KeyboardController;
import cz.kor3k.snake.controller.PlayerController;
import cz.kor3k.snake.controller.TouchController;
import cz.kor3k.snake.model.*;
import cz.kor3k.forge.Game;


/**
 * Created by martin on 28. 10. 2014.
 */
public class WorldScreen extends cz.kor3k.forge.WorldScreen
{
    protected World               world;
    private float ppuX;	// pixels per unit on the X axis
    private float ppuY;	// pixels per unit on the Y axis
    private int  matrixDotWidthPx   =    20;
    protected SnakeGame game  =   (SnakeGame) Game.obj;
    protected Settings  settings;

    public WorldScreen()
    {
        super( Game.obj );
        this.settings    =   game.settings;
    }

    @Override
    public Screen create()
    {
        super.create();

        this.world              =   new World( 40 , 30 , game );

        //set up keyboard controllers - general first, then players'
        InputMultiplexer input  =   new InputMultiplexer();
        input.addProcessor( new KeyboardController( this.world , this.game ) );
        input.addProcessor( new TouchController( this.world , this.game ) );
        Gdx.input.setInputProcessor( input );

        for( Settings.Player p : settings.getPlayers() )
        {
            Player  player =   new Player( p.id );
            PlayerController playerCtrl;

            if( p.leftKey != null && p.rightKey != null )
            {
                playerCtrl  =   new PlayerController.Keyboard( player , p.leftKey , p.rightKey );
                game.playerControllers.add( playerCtrl );
                input.addProcessor( playerCtrl );
            }

            if( p.leftArea != null && p.rightArea != null )
            {
                playerCtrl  =   new PlayerController.Touch( player , p.leftArea , p.rightArea );
                game.playerControllers.add( playerCtrl );
                input.addProcessor( playerCtrl );
            }

            this.world.players.add( player );

            //in NetworkServer, send right after creating
            //in NetworkClient, fetch World from server and replace it's players' controllers with local ones, and also in game.playerControllers
        }

        this.world.create();
        this.world.setGameState( Game.State.READY );
        Gdx.input.setCatchBackKey( true );

        return this;
    }

    public void updateModel( float delta )
    {
        if( this.world.isRunning() )
        {
            this.world.update( delta );
        }
    }

    protected Color getPlayerColor( Player player )
    {
        Color color;
        color   =   this.getPlayerSettings( player ).color;

        if( null == color )
            return game.assets.colors.get( "green" );
        else
            return color;
    }

    protected Settings.Player getPlayerSettings( Player player )
    {
        return settings.getPlayer( player.getId() );
    }

    protected void renderWorld()
    {
        for( Snake snake : world.snakes )
        {
            Color color =   this.getPlayerColor( snake.getPlayer() );

            for( Joint joint : snake.getJoints() )
            {
                this.draw( joint.bounds, color );
            }
        }

        for( Food food : world.foods )
        {
            this.draw( food.bounds , game.assets.colors.get( "yellow" ) );
        }

        for( Powerup powerup : world.powerups )
        {
            Color color;
            if( powerup instanceof Powerup.Chop || powerup instanceof Powerup.Blockade )
            {
                color   =   game.assets.colors.get( "yellow-green" );
            }
            else if( powerup instanceof Powerup.Confuse || powerup instanceof Powerup.Noclip || powerup instanceof Powerup.Tunnel )
            {
                color   =   game.assets.colors.get( "dark-green" );
            }
            else if( powerup instanceof Powerup.Fast || powerup instanceof Powerup.Slow || powerup instanceof Powerup.Shrink || powerup instanceof Powerup.Reverse )
            {
                color   =   game.assets.colors.get( "pink" );
            }
            else if( powerup instanceof Powerup.Freeze || powerup instanceof Powerup.SwapPlayers || powerup instanceof Powerup.SwapSnakes )
            {
                color   =   game.assets.colors.get( "light-green" );
            }
            else
            {
                color   =   game.assets.colors.get( "white" );
            }

            this.draw( new Circle( powerup.position , 1 ) , color );

        }

        for( Wall wall : world.walls )
        {
            for( Block block : wall.getBlocks() )
            {
                this.draw( block.bounds , game.assets.colors.get( "white" ) );
            }
        }

        for( Wormhole wormhole : world.wormholes )
        {
            this.draw( wormhole.getExitA().bounds , game.assets.colors.get( "light-green" ) );
            this.draw( wormhole.getExitB().bounds , game.assets.colors.get( "light-green" ) );
        }
    }

    protected void renderReady( float delta )
    {
        Color color     =   new Color( 1, 1, 1, 0.5f );
        super.draw("Snake Pit", color, new Vector2(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 2) , game.assets.fontBig );
//        super.draw("Game Ready", color, new Vector2(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 2));
        super.draw("ENTER / Touch : start", color, new Vector2(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 4) , game.assets.fontBold );
    }

    protected void draw( Texture texture , Vector2 position )
    {
        this.draw(texture, new Rectangle(position.x, position.y, 1, 1));
    }

    protected void draw( Rectangle rectangle , Color color )
    {
        int x = (int)rectangle.x * matrixDotWidthPx;
        int y = (int)rectangle.y * matrixDotWidthPx;
        int width   = (int)rectangle.width * matrixDotWidthPx;
        int height  = (int)rectangle.height * matrixDotWidthPx;
        super.draw( new Rectangle(x * ppuX, y * ppuY, width * ppuX, height * ppuY) , color );
    }

    protected void draw( Triangle triangle, Color color )
    {
        float x1 = triangle.x1 * matrixDotWidthPx;
        float y1 = triangle.y1 * matrixDotWidthPx;
        float x2 = triangle.x2 * matrixDotWidthPx;
        float y2 = triangle.y2 * matrixDotWidthPx;
        float x3 = triangle.x3 * matrixDotWidthPx;
        float y3 = triangle.y3 * matrixDotWidthPx;
        super.draw( new Triangle( x1, y1, x2, y2, x3, y3 ) , color );
    }

    protected void draw( Ellipse ellipse , Color color )
    {
        int x = (int)ellipse.x * matrixDotWidthPx;
        int y = (int)ellipse.y * matrixDotWidthPx;
        int width   = (int)ellipse.width * matrixDotWidthPx;
        int height  = (int)ellipse.height * matrixDotWidthPx;
        super.draw( new Ellipse(x * ppuX, y * ppuY, width * ppuX, height * ppuY) , color );
    }

    protected void draw( Circle circle , Color color )
    {
        this.draw( new Ellipse( circle ) , color );
    }

    protected void draw( Texture texture , Rectangle position )
    {
        int x = (int)position.x * matrixDotWidthPx;
        int y = (int)position.y * matrixDotWidthPx;
        int width   = (int)position.width * matrixDotWidthPx;
        int height  = (int)position.height * matrixDotWidthPx;
        super.draw(texture, new Rectangle(x * ppuX, y * ppuY, width * ppuX, height * ppuY));
    }

    protected void draw( CharSequence text , Color color , Vector2 position )
    {
        this.draw( text , color , position , game.font );
    }

    protected void draw(CharSequence text, Color color, Vector2 position, BitmapFont font )
    {
        int x = (int)position.x * matrixDotWidthPx;
        int y = (int)position.y * matrixDotWidthPx;
        super.draw(text, color, new Vector2( x , y ) , font );
    }

    protected void renderHud()
    {
        Color color;

        color =   new Color( game.assets.colors.get( "white" ) );
        color.a     =   0.2f;
        game.assets.fontSmall.setColor(color);

//        super.draw("Game Running", color, new Vector2(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 2));
//        super.draw("O : game over", color, new Vector2(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 3));
//        super.draw("P / ESC : pause", color, new Vector2(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 4));

        for( int i = 0 ; i < world.players.size() ; i++  )
        {
            Player  player  =   world.players.get(i);
            int     y       =   Gdx.graphics.getHeight() - ( 30 * i + 5 );

            color       =   this.getPlayerColor( player );

            game.assets.fontSmall.setColor( color );
            game.assets.fontSmall.draw( game.batch,
                    this.getPlayerSettings( player ).name + " score: " + player.getScore() , 5 , Gdx.graphics.getHeight() - ( 30 * i + 5 ) );

//            game.assets.fontSmall.draw( game.batch,
//                    "confuse" , 140 , y );
//            game.assets.fontSmall.draw( game.batch,
//                    "noclip" , 210 , y );
//            game.assets.fontSmall.draw( game.batch,
//                    "freeze" , 270 , y );
//            game.assets.fontSmall.draw( game.batch,
//                    "speed" , 330 , y );

            if( player.getSnake().new Buffs().hasConfuse() )
            {
                game.assets.fontSmall.draw( game.batch,
                        "confuse" , 140 , y );
            }

            if( player.getSnake().new Buffs().hasNoclip() )
            {
                game.assets.fontSmall.draw( game.batch,
                        "noclip" , 210 , y );
            }

            if( player.getSnake().new Buffs().hasFreeze() )
            {
                game.assets.fontSmall.draw( game.batch,
                        "freeze" , 270 , y );
            }

            else if( player.getSnake().new Buffs().hasSpeed() )
            {
                game.assets.fontSmall.draw( game.batch,
                        "speed" , 330 , y );
            }
        }
    }

    protected void renderRunning( float delta )
    {
        this.renderHud();
        this.renderWorld();
    }

    private int getPausedInfoPosition( int order )
    {
        return 29 - order * 2;
    }

    protected void renderPaused( float delta )
    {
        this.draw( "confuse" , game.assets.colors.get( "dark-green" ) , new Vector2( 1 , getPausedInfoPosition(0) ) , game.assets.fontBold );
        this.draw( Powerup.Confuse.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(0) ) );
        this.draw( "noclip" , game.assets.colors.get( "dark-green" ) , new Vector2( 1 , getPausedInfoPosition(1) ) , game.assets.fontBold );
        this.draw( Powerup.Noclip.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(1) ) );
        this.draw( "tunnel" , game.assets.colors.get( "dark-green" ) , new Vector2( 1 , getPausedInfoPosition(2) ) , game.assets.fontBold );
        this.draw( Powerup.Tunnel.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(2) ) );
        this.draw( "reverse" , game.assets.colors.get( "pink" ) , new Vector2( 1 , getPausedInfoPosition(3) ) , game.assets.fontBold );
        this.draw( Powerup.Reverse.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(3) ) );
        this.draw( "shrink" , game.assets.colors.get( "pink" ) , new Vector2( 1 , getPausedInfoPosition(4) ) , game.assets.fontBold );
        this.draw( Powerup.Shrink.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(4) ) );
        this.draw( "fast" , game.assets.colors.get( "pink" ) , new Vector2( 1 , getPausedInfoPosition(5) ) , game.assets.fontBold );
        this.draw( Powerup.Fast.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(5) ) );
        this.draw( "slow" , game.assets.colors.get( "pink" ) , new Vector2( 1 , getPausedInfoPosition(6) ) , game.assets.fontBold );
        this.draw( Powerup.Slow.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(6) ) );
        this.draw( "blockade" , game.assets.colors.get( "yellow-green" ) , new Vector2( 1 , getPausedInfoPosition(7) ) , game.assets.fontBold );
        this.draw( Powerup.Blockade.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(7) ) );
        this.draw( "chop" , game.assets.colors.get( "yellow-green" ) , new Vector2( 1 , getPausedInfoPosition(8) ) , game.assets.fontBold );
        this.draw( Powerup.Chop.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(8) ) );
        this.draw( "freeze" , game.assets.colors.get( "light-green" ) , new Vector2( 1 , getPausedInfoPosition(9) ) , game.assets.fontBold );
        this.draw( Powerup.Freeze.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 7 , getPausedInfoPosition(9) ) );
        this.draw( "swap players" , game.assets.colors.get( "light-green" ) , new Vector2( 1 , getPausedInfoPosition(10) ) , game.assets.fontBold );
        this.draw( Powerup.SwapPlayers.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 9 , getPausedInfoPosition(10) ) );
        this.draw( "swap snakes" , game.assets.colors.get( "light-green" ) , new Vector2( 1 , getPausedInfoPosition(11) ) , game.assets.fontBold );
        this.draw( Powerup.SwapSnakes.getInfo() , game.assets.colors.get( "white" ) , new Vector2( 9 , getPausedInfoPosition(11) ) );


//        Color color =   new Color( 1 , 1 , 1 , 0.2f );
//        super.draw( "Game Paused" , color , new Vector2( Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 2 )  );
//        super.draw( "Q : quit" , color , new Vector2( Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 3 )  );
//        super.draw( "P / ESC : unpause" , color , new Vector2( Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 4 )  );
    }

    protected void renderOver( float delta )
    {
        Color color     =   new Color( 1, 1, 1, 0.5f );
        super.draw( "Game Over" , color , new Vector2( Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 2) );
        super.draw( "ENTER : restart" , color , new Vector2( Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 3) );
        super.draw( "ESC : quit" , color , new Vector2( Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 4) );

        for( int i = 0 ; i < world.players.size() ; i++  )
        {
            Player player = world.players.get(i);

            color       =   this.getPlayerColor( player );

            super.draw(
                    this.getPlayerSettings( player ).name + " score: " + player.getScore() ,
                    color ,
                    new Vector2( Gdx.graphics.getWidth() / 3 , Gdx.graphics.getHeight() - ( i * 30 + 10 )  )
            );
        }
    }

    public void renderView( float delta )
    {
        game.clearScreen(0.0f, 0.0f, 0.2f, 1);

        game.font.setColor(1, 1, 1, 1);
//        game.font.setScale(2);

        game.batch.begin();
        game.shape.begin( ShapeRenderer.ShapeType.Filled );

        if( this.world.isReady() )
        {
            this.renderReady( delta );
        }

        else if( this.world.isRunning() )
        {
            this.renderRunning( delta );
        }

        else if( this.world.isPaused() )
        {
            this.renderPaused( delta );
        }

        else if( this.world.isOver() )
        {
            this.renderOver( delta );
        }

        game.batch.end();
        game.shape.end();
    }

    public void resize(int width, int height)
    {
        //use ScalingViewport instead of this shit

        Gdx.app.debug("GameScreen", "resize()ing");
        //this.ratio = this.width/this.height;
        ppuX = (float)width / ( (float)world.getMatrixWidth() * (float)matrixDotWidthPx );
        Gdx.app.debug("GameScreen", "resize().ppuX: "+ppuX);
        ppuY = (float)height / ( (float)world.getMatrixHeight() * (float)matrixDotWidthPx );
        Gdx.app.debug("GameScreen", "resize().ppuY: "+ppuY);
//        game.batch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);

        super.resize( width , height );
    }

    public void pause()
    {
        super.pause();
        this.world.pause();
    }

    public void resume()
    {
        super.resume();
        this.world.resume();
    }
}
