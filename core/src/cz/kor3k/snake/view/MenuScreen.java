package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.UIUtils;
import com.badlogic.gdx.utils.Timer;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.Scene2dUiScreen;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.Settings;
import cz.kor3k.snake.SnakeGame;
import cz.kor3k.snake.model.Powerup;

/**
 * Created by martin on 23. 11. 2014.
 */
public class MenuScreen extends Scene2dUiScreen
{
    protected SnakeGame game    =   (SnakeGame) Game.obj;

    public MenuScreen()
    {
        super( Game.obj );
    }

    protected TextButton createMainMenuButton()
    {
        TextButton mainMenuButton = new TextButton( "main menu" , skin);
        mainMenuButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "click " + x + ", " + y);
                        game.setScreen( new MenuScreen().create() );
                    }
                });

        return mainMenuButton;
    }

    protected TextButton createBackButton( final MenuScreen screen )
    {
        TextButton backButton = new TextButton( "back" , skin);
        backButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "back " + x + ", " + y);
                        game.setScreen( screen.create() );
                    }
                });

        return backButton;
    }

    protected Label createLabel( String text , String color )
    {
        return super.createLabel( text , game.assets.colors.get( color ) );
    }

    public Screen create ()
    {


//        pane.setFillParent( true );
//        table.setFillParent( true );

        this.prepareUi( game.assets.uiskin );
        container.add( scrollPane ).expand().fill();
        container.row().space(10).padBottom(10);
//        container.add( createMainMenuButton() ).center();
//        container.add( createMainMenuButton() ).center().expand().fill();

//        for( int i = 0 ; i < 1 ; i++ )
//            table
//                .add(new Label(i + "tres long0 long1 long2 long3 long4 long5 long6 long7 long8 long9 long10 long11 long12", skin))
//                .row();

        skin.get( TextButton.TextButtonStyle.class ).font   =   game.assets.fontBold;
        skin.get( Label.LabelStyle.class ).font             =   game.assets.fontRegular;

        TextButton playersButton = new TextButton( "settings" ,  skin);
        playersButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "local players " + x + ", " + y);
                        game.setScreen( new PlayersMenuScreen().create() );
                    }
                });

        TextButton playButton = new TextButton( "play" , skin);
        playButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "play " + x + ", " + y);
                        game.setScreen( new WorldScreen().create()  );
                    }
                });

        TextButton infoButton = new TextButton( "wtf" ,  skin);
        infoButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "info " + x + ", " + y);
                        game.setScreen( new InfoMenuScreen().create() );
                    }
                });

        TextButton hostNetButton = new TextButton( "host net" ,  skin);
        hostNetButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "info " + x + ", " + y);
                        game.setScreen( new ServerScreen.StartServerScreen().create() );
                    }
                });

        TextButton joinNetButton = new TextButton( "join net" ,  skin);
        joinNetButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "info " + x + ", " + y);
                        game.setScreen( new ClientScreen.StartClientScreen().create() );
                    }
                });

        TextButton exitButton = new TextButton( "exit" , skin);
        exitButton.addListener(
                new ClickListener()
                {
                    public void clicked (InputEvent event, float x, float y)
                    {
                        Gdx.app.log( "MenuScreen" , "exit " + x + ", " + y);
                        Gdx.app.exit();
                    }
                });

        Label.LabelStyle    ls      =   new Label.LabelStyle( skin.get( Label.LabelStyle.class )  );
        ls.font                     =   game.assets.fontBig;
        Label title                 =   new Label( "Snake" , ls );

        table.add( title );
        table.row();
        table.add( createLabel( "v0.2.4" , game.assets.colors.get( "white" ) , game.assets.fontSmall ) );
        table.row();
        table.add( playButton );
        table.row();
        table.add( playersButton );
        table.row();
        table.add( infoButton );
        table.row();
        table.add( hostNetButton );
        table.row();
        table.add( joinNetButton );
        table.row();
        table.add( exitButton );

        return this;
    }

    protected static class EditPlayerMenuScreen extends AddPlayerMenuScreen
    {

        public EditPlayerMenuScreen( Settings.Player player )
        {
            super();
            super.player    =   player;
        }
    }

    protected static class AddPlayerMenuScreen extends MenuScreen
    {
        protected Settings.Player player;

        //TODO: first two also get touch areas

        public AddPlayerMenuScreen()
        {
            super();
            createPlayer();
        }

        private void createPlayer()
        {
            player      =   new Settings.Player();
            player.name =   "Player " + ( game.settings.getPlayers().size() + 1 );

            if( 1 == game.settings.getPlayers().size() )
            {
                player.leftKey  =   Input.Keys.J;
                player.rightKey =   Input.Keys.K;
                player.leftArea  =   new Rectangle();
                player.rightArea =   new Rectangle();
            }
        }

        protected boolean isKeyAllowed( int keycode )
        {
            if( !UIUtils.alt() && !UIUtils.ctrl() && !UIUtils.shift()
                    && keycode != Input.Keys.ESCAPE && keycode != Input.Keys.ENTER
                    && keycode != Input.Keys.TAB && keycode != Input.Keys.SYM && keycode != Input.Keys.UNKNOWN )
                return true;
            else
                return false;
        }

        protected TextButton createSavePlayerButton( final TextField[] textFields )
        {
            TextButton savePlayerButton = new TextButton( "save player" , skin);
            savePlayerButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            for( TextField tf : textFields )
                            {
                                if( 0 == tf.getText().length() )
                                {
                                    return;
                                }
                            }

                            player.name     =   textFields[0].getText();
                            player.leftKey  =   Input.Keys.valueOf( textFields[1].getText() );
                            player.rightKey =   Input.Keys.valueOf( textFields[2].getText() );

                            try
                            {
                                player.color    =   Color.valueOf( textFields[3].getText() );
                            }
                            catch( NumberFormatException e )
                            {

                            }


                            if( !game.settings.getPlayers().contains( player ) )
                            {
                                game.settings.addPlayer( player );
                                game.resize( game.getScreenWidth() , game.getScreenHeight() ); //this is because of touch areas
                            }

                            Gdx.app.log( "MenuScreen" , "save player " + x + ", " + y);
                            game.setScreen( new PlayersMenuScreen().create() );
                        }
                    });

            return savePlayerButton;
        }

        protected TextField[] createPlayerForm()
        {
            final TextField[] textFields  =   new TextField[4];

            textFields[0]   =   new TextField( "name" , skin );
            textFields[1]   =   new TextField( "left key" , skin );
            textFields[2]   =   new TextField( "right key" , skin );
            TextField.TextFieldStyle ts =   new TextField.TextFieldStyle( skin.get( TextField.TextFieldStyle.class ) );
            if( null != player.color )
                ts.fontColor    =   player.color;
            textFields[3]   =   new TextField( "color" , ts );

            textFields[0].setText( this.player.name );
            textFields[1].setText(null == player.leftKey ? "" : Input.Keys.toString(player.leftKey));
            textFields[1].setMessageText("<set left key>");
            textFields[2].setText(null == player.rightKey ? "" : Input.Keys.toString(player.rightKey));
            textFields[2].setMessageText("<set right key>");

            textFields[3].setText( null == this.player.color ? "" : this.player.color.toString().substring( 0 , 6 ) );
            textFields[3].setMessageText("<color>");

            textFields[1].addListener(
                    new InputListener()
                    {
                        @Override
                        public boolean keyDown(InputEvent event, int keycode)
                        {
                            if( isKeyAllowed( keycode ) )
                            {
                                player.leftKey  =   keycode;
                                Timer.schedule(
                                        new Timer.Task() {
                                            public void run()
                                            {
                                                textFields[1].setText(Input.Keys.toString(player.leftKey));
                                            }
                                        }, 0.1f);
                            }

                            return true;
                        }
                    });

            textFields[2].addListener(
                    new InputListener()
                    {
                        @Override
                        public boolean keyDown(InputEvent event, int keycode)
                        {
                            if( isKeyAllowed( keycode ) )
                            {
                                player.rightKey  =   keycode;
                                Timer.schedule(
                                        new Timer.Task() {
                                            public void run()
                                            {
                                                textFields[2].setText(Input.Keys.toString(player.rightKey));
                                            }
                                        }, 0.1f);
                            }

                            return true;
                        }
                    });

            return textFields;
        }

        @Override
        public Screen create()
        {
            this.prepareUi( game.assets.uiskin );
            container.add(scrollPane).expand().fill().colspan(3);
            container.row().space(10).padBottom(10);
            container.add( createBackButton( new PlayersMenuScreen() ) );
            container.add( createMainMenuButton() );

            TextField[] tf  =   createPlayerForm();

            final TextField   name      =   tf[0];
            final TextField   leftKey   =   tf[1];
            final TextField   rightKey  =   tf[2];
            final TextField   color     =   tf[3];

            table.add( new Label( "name" , skin ) );
            table.add( name );
            table.row();
            table.add( new Label( "left key" , skin ) );
            table.add( leftKey );
            table.row();
            table.add( new Label( "right key" , skin ) );
            table.add( rightKey );
            table.row();

            table.add( new Label( "color" , skin ) );
            table.add( color );
            table.row();

            container.add( createSavePlayerButton( tf ) );

            return this;
        }
    }

    protected static class InfoMenuScreen extends MenuScreen
    {

        public InfoMenuScreen()
        {
            super();
        }

        @Override
        public Screen create()
        {
            this.prepareUi( game.assets.uiskin );
            container.add(scrollPane).expand().fill().colspan(1);
            container.row().space(10).padBottom(10);
            container.add(createMainMenuButton()).center();

//            table.add( createLabel( "turn snake left or right. first two players also have touch controls.",
//                    game.assets.colors.get( "white"), game.assets.fontRegular ) )
//                 .colspan(2).left().row();
//
//            table.add( createLabel( "there is regular food and also powerups.",
//                    game.assets.colors.get( "white"), game.assets.fontRegular ) )
//                 .colspan(2).left().row();
//
//            table.add( createLabel( "play multiplayer games via tcp/udp or websockets.",
//                    game.assets.colors.get( "white"), game.assets.fontRegular ) )
//                 .colspan(2).left().row();

            createPowerupTable(this.table);

            Label createdBy =   createLabel("created by kor3k",
                    new Color( 0.8f , 0.8f , 1.0f , 1.0f ) , game.assets.fontRegular);
            createdBy.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            Gdx.net.openURI( "https://facebook.com/hemperor" );
                        }
                    });

            Label license =   createLabel("this software is distributed under GNU General Public License v3",
                    game.assets.colors.get( "white" ) , game.assets.fontRegular);
            license.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            Gdx.net.openURI( "http://www.gnu.org/copyleft/gpl.html" );
                        }
                    });

            table.add( createdBy )
                    .colspan(2).row();
            table.add( license )
                    .colspan(2).row();

            return this;
        }

        protected Label createLabel( String text , String color )
        {
            Label label = super.createLabel( text , game.assets.colors.get( color ) );

            Label.LabelStyle ls =   new Label.LabelStyle( label.getStyle() );
            ls.font   =   game.assets.fontBold;
            label.setStyle( ls );

            return label;
        }

        private Table createPowerupTable( Table table )
        {
            table.add( createLabel( "confuse" , "dark-green" ) ).left();
            table.add( Powerup.Confuse.getInfo() ).left();
            table.row();
            table.add( createLabel( "noclip" , "dark-green" ) ).left();
            table.add( Powerup.Noclip.getInfo() ).left();
            table.row();
            table.add( createLabel( "tunnel" , "dark-green" ) ).left();
            table.add( Powerup.Tunnel.getInfo() ).left();
            table.row();
            table.add( createLabel( "reverse" , "pink") ).left();
            table.add( Powerup.Reverse.getInfo() ).left();
            table.row();
            table.add( createLabel( "shrink" , "pink" ) ).left();
            table.add( Powerup.Shrink.getInfo() ).left();
            table.row();
            table.add( createLabel( "fast" , "pink" ) ).left();
            table.add( Powerup.Fast.getInfo() ).left();
            table.row();
            table.add( createLabel( "slow" , "pink" ) ).left();
            table.add( Powerup.Slow.getInfo() ).left();
            table.row();
            table.add( createLabel( "blockade" , "yellow-green" ) ).left();
            table.add( Powerup.Blockade.getInfo() ).left();
            table.row();
            table.add( createLabel( "chop" , "yellow-green" ) ).left();
            table.add( Powerup.Chop.getInfo() ).left();
            table.row();
            table.add( createLabel( "freeze" , "light-green" ) ).left();
            table.add( Powerup.Freeze.getInfo() ).left();
            table.row();
            table.add( createLabel( "swap players" , "light-green" ) ).left();
            table.add( Powerup.SwapPlayers.getInfo() ).left();
            table.row();
            table.add( createLabel( "swap snakes" , "light-green" ) ).left();
            table.add( Powerup.SwapSnakes.getInfo() ).left();
            table.row();

            return table;
        }
    }


    protected static class PlayersMenuScreen extends MenuScreen
    {

        public PlayersMenuScreen()
        {
            super();
        }

        @Override
        public Screen create()
        {

            this.prepareUi( game.assets.uiskin );
            container.add( scrollPane ).expand().fill().colspan(2);
            container.row().space(10).padBottom(10);
            container.add(createMainMenuButton());

            TextButton addPlayerButton = new TextButton( "add player" , skin);
            addPlayerButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            Gdx.app.log( "MenuScreen" , "add player " + x + ", " + y);
                            game.setScreen( new AddPlayerMenuScreen().create() );
                        }
                    });

            container.add(addPlayerButton);

            for( final Settings.Player p : game.settings.getPlayers() )
            {
                TextField.TextFieldStyle    ts      =   new TextField.TextFieldStyle( skin.get( TextField.TextFieldStyle.class ) );
                ts.fontColor                        =   game.settings.getPlayer( p.id ).color;
                TextField                   name    =   new TextField( p.name , ts );
                name.setDisabled( true );

                TextButton editPlayerButton = new TextButton( "edit player" , skin );
                editPlayerButton.addListener(
                        new ClickListener()
                        {
                            public void clicked (InputEvent event, float x, float y) {
                                Gdx.app.log( "MenuScreen" , "edit player " + x + ", " + y);
                                game.setScreen( new EditPlayerMenuScreen( p ).create() );
                            }
                        });

                TextButton removePlayerButton = new TextButton( "remove player" , skin );
                removePlayerButton.addListener(
                        new ClickListener()
                        {
                            public void clicked (InputEvent event, float x, float y)
                            {
                                Gdx.app.log( "MenuScreen" , "remove player " + x + ", " + y);
                                game.settings.getPlayers().remove( p );
                                game.setScreen( new PlayersMenuScreen().create() );
                            }
                        });

                table.add( name );
                table.add( editPlayerButton );
                table.add( removePlayerButton );
                table.row();

                if( 0 == game.settings.getPlayers().indexOf( p ) )
                {
                    removePlayerButton.remove();
                }
            }

            return this;
        }
    }

    @Override
    public void render ( float delta )
    {
        super.render( delta );

//        if ( Gdx.input.isKeyPressed( Input.Keys.ESCAPE )
//                || Gdx.input.isKeyPressed( Input.Keys.Q )
//                || Gdx.input.isKeyPressed( Input.Keys.E )
//                || Gdx.input.isKeyPressed( Input.Keys.BACK ) )
//        {
//            Gdx.app.exit();
//        }

//        if ( Gdx.input.isKeyPressed( Input.Keys.ENTER ) )
//        {
//            game.setScreen( new WorldScreen( (SnakeGame)game ) );
//        }
    }

    public void render2( float delta )
    {

        game.clearScreen(0.0f, 0.0f, 0.2f, 1);

        //initialize WsServer
        //listen for connections. accept user settings (name + id), add users
        //bind network controllers to network users
        //when all connected, send gamestart and set screen to new ServerWorldScreen( WsServer )
    }
}
