package cz.kor3k.snake.view;

import com.badlogic.gdx.graphics.Texture;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.SnakeGame;

/**
 * Created by martin on 29. 10. 2014.
 */
public class LoadingScreen extends Screen
{
    public LoadingScreen(Game game) {
        super(game);
    }

    public void render( float delta )
    {
        SnakeGame game = (SnakeGame)this.game;
        this.loadAssets();
        System.out.println(game.assets.getProgress());
        System.out.println(game.assets.isLoaded( "joint.png" ));

        game.clearScreen(0.0f, 0.0f, 0.2f, 1);
        game.batch.begin();
        game.font.setColor(30, 30, 30, 100);
        game.font.setScale(2);
        game.font.draw(game.batch, "loading: " + game.assets.getProgress() + "%" , 50 , 50 );
        game.batch.end();

        if ( 100f == game.assets.getProgress() )
        {
            game.setScreen( new OldMenuScreen( game ) );
            game.clearScreen(0.0f, 0.0f, 0.2f, 1);
            this.dispose();
        }
    }

    private void loadAssets()
    {
        SnakeGame game = (SnakeGame)this.game;

        game.assets.load( "joint.png" , Texture.class );
    }
}
