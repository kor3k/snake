package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.Scene2dUiScreen;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.SnakeGame;

/**
 * Created by martin on 30. 11. 2014.
 */
public class StartScreen extends Scene2dUiScreen
{
    public StartScreen(Game game)
    {
        super(game);
    }

    @Override
    public Screen create()
    {
        SnakeGame game =    (SnakeGame)this.game;

        this.prepareUi( game.assets.uiskin );
        container.add(scrollPane).expand().fill().colspan(1);
        container.row().space(10).padBottom(10);

        Image               logo    =   new Image( game.assets.logo );

        Label.LabelStyle    ls      =   new Label.LabelStyle( skin.get( Label.LabelStyle.class )  );
        ls.font                     =   game.assets.fontBig;
        Label title                 =   new Label( "MadBiatch Games" , ls );

        table.add( logo ).center();
        table.row();
        table.add( title );
        table.row();
        table.add( createLabel(
                                "high as a kite" ,
                                game.assets.colors.get( "white" ) ,
                                game.assets.createFont( 15 , game.assets.fontRegularFile ) )
                );

        return this;
    }

    @Override
    public void render(float delta)
    {
        super.render(delta);

        if( Gdx.input.justTouched() || Gdx.input.isKeyJustPressed( Input.Keys.ANY_KEY ) )
        {
            game.setScreen( new MenuScreen().create() );
        }
    }
}
