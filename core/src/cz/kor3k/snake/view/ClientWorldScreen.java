package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import cz.kor3k.forge.Game;
import cz.kor3k.snake.Net;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.Settings;
import cz.kor3k.snake.controller.PlayerController;
import cz.kor3k.snake.model.*;


/**
 * Created by martin on 16. 11. 2014.
 */
public class ClientWorldScreen extends WorldScreen
{
    private Net.Client      client;

    public ClientWorldScreen( Net.Client client )
    {
        super();
        this.client =   client;
    }

    @Override
    public Screen create()
    {
        super.settings  =   game.serverSettings;

        this.world              =   new World( 40 , 30 , game );

        Gdx.app.log( "ClientWorldScreen" , "main thread name: " + Thread.currentThread().getName() );

        this.world.setGameState( Game.State.READY );
        Gdx.input.setCatchBackKey( true );

        InputMultiplexer input  =   new InputMultiplexer();
        Gdx.input.setInputProcessor( input );

        for( Settings.Player p : game.settings.getPlayers() )
        {
            PlayerController playerCtrl;

            if( p.leftKey != null && p.rightKey != null )
            {
                playerCtrl  =   new PlayerController.Network.Client( client ).new Keyboard( new Player().setId( p.id ) , p.leftKey , p.rightKey );
                game.playerControllers.add( playerCtrl );
                input.addProcessor( playerCtrl );
            }

            if( p.leftArea != null && p.rightArea != null )
            {
                playerCtrl  =   new PlayerController.Network.Client( client ).new Touch( new Player().setId( p.id ) , p.leftArea , p.rightArea );
                game.playerControllers.add( playerCtrl );
                input.addProcessor( playerCtrl );
            }
        }

        return this;
    }

    @Override
    protected Settings.Player getPlayerSettings(Player player)
    {
        return game.serverSettings.getPlayer( player.getId() );
    }

    @Override
    protected void renderWorld()
    {
        super.renderWorld();
    }

    @Override
    public void resize(int width, int height)
    {
        if( null == this.world )
            return;

        super.resize(width, height);
    }

    @Override
    public void updateModel(float delta)
    {
        if( game.flagExit )
        {
            game.flagExit   =   false;
            exitGame();
        }

        if( null != game.remoteWorld )
        {
            this.updateModel( game.remoteWorld );
            game.remoteWorld    =   null;
        }
    }

    @Override
    public void renderView(float delta)
    {
        if( null == this.world )
            return;

        super.renderView(delta);
    }

    @Override
    public void pause()
    {
        if( null == this.world )
            return;

        super.pause();
    }

    @Override
    public void resume()
    {
        if( null == this.world )
            return;

        super.resume();
    }

    private void exitGame()
    {
        game.setScreen( new MenuScreen().create() );
    }

    public synchronized void updateModel( World remoteWorld )
    {

        for( Powerup p : remoteWorld.powerups )
        {
            if( p instanceof Powerup.Blockade )
            {
                ((Powerup.Blockade) p).setWorld( remoteWorld );
            }

            if( p instanceof Powerup.Tunnel )
            {
                ((Powerup.Tunnel) p).setWorld( remoteWorld );
            }
        }

        remoteWorld.snakes.clear();
        for( Player p : remoteWorld.players )
        {
            p.getSnake().setPlayer(p);
            remoteWorld.snakes.add( p.getSnake() );

            for( Snake.Buff b : p.getSnake().getBuffs() )
            {
                b.setSnake( p.getSnake() );
            }
        }

        //TODO: player controllers
        for( Player p : remoteWorld.players )
        {
            for( PlayerController ctrl : game.playerControllers )
            {
                if( p.equals( ctrl.getPlayer() ) )
                {
                    ctrl.setPlayer(p);
                }

            }
        }

        remoteWorld.game    =   game;
        this.world          =   remoteWorld;
    }

    @Override
    public void dispose()
    {
        client.dispose();
        super.dispose();
    }
}
