package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import cz.kor3k.snake.Net;
import cz.kor3k.forge.Screen;

/**
 * Created by martin on 23. 11. 2014.
 */
public abstract class ClientScreen extends MenuScreen
{
    public ClientScreen()
    {
        super();
    }

    @Override
    public Screen create()
    {
        return this;
        //initialize WsClient
        //connect to server
        //send local player settings (name + id)
        //bind controllers to users
        //wait for gamestart, then set screen to new ClientWorldScreen( WsClient )
    }

    public static class StartClientScreen extends ClientScreen
    {
        public StartClientScreen()
        {
            super();
        }

        public Screen create()
        {
            this.prepareUi( game.assets.uiskin );
            container.add(scrollPane).expand().fill().colspan(3);
            container.row().space(10).padBottom(10);

            final TextField   hostname    =   new TextField( "" , skin );
            hostname.setMessageText( "<enter hostname>" );

            final TextField   port        =   new TextField( "8025" , skin );
            port.setMessageText( "<enter port>" );

            table.add( "hostname" ).right();
            table.add( hostname ).left();
            table.row();
            table.add( "port" ).right();
            table.add( port ).left();
            table.row();

            final TextButton startKryonetClientButton = new TextButton( "start kryonet" , skin);
            startKryonetClientButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            if( 0 == hostname.getText().length() || 0 == port.getText().length() )
                            {
                                return;
                            }

                            startKryonetClientButton.setDisabled(true);

                            Net.Client client   =   new cz.kor3k.snake.kryonet.Factory().createClient( hostname.getText() , port.getText() );
                            client.create();

                            Gdx.app.log("ClientScreen", "start client " + x + ", " + y);
                            game.setScreen(new ListenClientScreen( client ).create());
                        }
                    });

            final TextButton startWebsocketsClientButton = new TextButton( "start websockets" , skin);
            startWebsocketsClientButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            if( 0 == hostname.getText().length() || 0 == port.getText().length() )
                            {
                                return;
                            }

                            startWebsocketsClientButton.setDisabled(true);

                            Net.Client client   =   new cz.kor3k.snake.websockets.Factory().createClient( hostname.getText() , port.getText() );
                            client.create();

                            Gdx.app.log("ClientScreen", "start client " + x + ", " + y);
                            game.setScreen(new ListenClientScreen( client ).create());
                        }
                    });

            container.add( createMainMenuButton() );
            container.add( startWebsocketsClientButton );
            container.add( startKryonetClientButton );

            return this;
        }
    }

    public static class ListenClientScreen extends ClientScreen
    {
        protected Net.Client    client;

        public ListenClientScreen( Net.Client client )
        {
            super();
            this.client            =   client;
        }

        private void startGame()
        {
            game.setScreen( new ClientWorldScreen( client ).create() );
        }

        private void exitGame()
        {
            game.setScreen( new MenuScreen().create() );
        }

        @Override
        public void render( float delta )
        {
            if( game.flagExit )
            {
                game.flagExit   =   false;
                exitGame();
            }
            if( game.flagStart )
            {
                game.flagStart  =   false;
                startGame();
            }

            table.clear();

            String[]    url = Net.parseUrl( client.getUrl().toString() );

            table.add(url[1] + " : " + url[2]).colspan(2).fill();
            table.row();

            super.render( delta );
        }

        @Override
        public Screen create()
        {
            this.prepareUi( game.assets.uiskin );
            container.add(scrollPane).expand().fill().colspan(1);
            container.row().space(10).padBottom(10);

            TextButton stopClientButton = new TextButton( "stop client" , skin);
            stopClientButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            client.dispose();

                            Gdx.app.log("ClientScreen", "stop client " + x + ", " + y);
                            game.setScreen(new StartClientScreen().create());
                        }
                    });

            container.add( stopClientButton );

            return this;
        }
    }
}
