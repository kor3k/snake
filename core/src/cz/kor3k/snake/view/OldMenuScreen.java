package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.SnakeGame;

import java.util.Arrays;

public class OldMenuScreen extends Screen
{
    public OldMenuScreen(SnakeGame game)
    {
        super( game );
    }

    public void render( float delta )
    {
        SnakeGame game  =   (SnakeGame) this.game;

        game.clearScreen( 0.0f , 0.0f , 0.2f , 1 );

        game.batch.begin();

        game.font.setColor(30, 30, 30, 100);
//        game.font.setScale(2);


//        game.font.draw( game.batch , "Press any key", Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 2);

        Color color     =   new Color( 1, 1, 1, 0.8f );
        this.draw( game.assets.logo , new Rectangle( Gdx.graphics.getWidth() / 10  , Gdx.graphics.getHeight() - ( Gdx.graphics.getHeight() / 1.7f ) , 144 , 144 ) );
        this.draw("CrazyBiatch Games", color, new Vector2((Gdx.graphics.getWidth() / 10) + 150, Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() / 1.7f) + 82), game.assets.fontBig);

        game.batch.end();

        if ( Gdx.input.isKeyJustPressed( Input.Keys.E ) )
        {
            Gdx.app.exit();
//            game.pause();

//            game.resume();
            this.dispose();
        }

        if ( Gdx.input.isKeyJustPressed( Input.Keys.ANY_KEY ) || Gdx.input.justTouched() )
        {
//            game.setScreen(new WorldScreen(game));

//            if( 0 == game.settings.network )
//            {
//                game.setScreen( new WorldScreen( game ) );
//            }
//            else if( 1 == game.settings.network )
//            {
//                game.setScreen( new ServerWorldScreen( game ) );
//            }
//            else if( 2 == game.settings.network )
//            {
//                game.setScreen( new ClientWorldScreen( game ) );
//            }

            game.clearScreen(0.0f, 0.0f, 0.2f, 1);
            this.dispose();
        }
    }

    public void render2(float delta) {

        this.game.clearScreen( 0.0f , 0.0f , 0.2f , 1 );


        System.out.println(game.camera.viewportWidth);
        System.out.println(game.camera.viewportHeight);
        System.out.println( game.camera.position );


        game.batch.begin();
        game.font.setColor(30, 30, 30, 100);
        game.font.setScale(2);
        game.font.draw(game.batch, "Welcome to Drop!!! ", 100, 150);
        game.font.draw(game.batch, "Tap anywhere to begin!", 100, 100);
        System.out.println("klfjslkjlk");
        Gdx.graphics.setTitle( "Snake @ " + Integer.toString( Gdx.graphics.getFramesPerSecond() ) + " fps" );
        game.batch.end();

        System.out.println( Gdx.graphics.getDisplayModes()[Gdx.graphics.getDisplayModes().length - 1] );
        System.out.println( Gdx.graphics.getDisplayModes()[0] );
        System.out.println(Arrays.deepToString( Gdx.graphics.getDisplayModes() ) );
        Gdx.graphics.setDisplayMode( Gdx.graphics.getDisplayModes()[2] );
        System.out.println( Gdx.graphics.getDesktopDisplayMode() );

        if (Gdx.input.isTouched()) {
//            game.setScreen(new GameScreen(game));
            System.out.println( "TOOOOOOOUUUCH" );

            dispose();
        }


        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
//            game.setScreen(new GameScreen(game));
            System.out.println( "LEEEEEEEEEEEEEEEEEFT" );
            dispose();
        }
    }

}