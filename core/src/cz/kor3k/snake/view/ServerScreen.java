package cz.kor3k.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import cz.kor3k.snake.Net;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.SnakeGame;
import java.util.List;

/**
 * Created by martin on 23. 11. 2014.
 */
public abstract class ServerScreen extends MenuScreen
{
    public ServerScreen()
    {
        super();
    }

    @Override
    public Screen create()
    {
        return this;
        //initialize WsServer
        //listen for connections. accept user settings (name + id), add users
        //bind network controllers to network users
        //when all connected, send gamestart and set screen to new ServerWorldScreen( WsServer )
    }

    public static class StartServerScreen extends ServerScreen
    {
        public StartServerScreen()
        {
            super();
        }

        public Screen create()
        {
            this.prepareUi( game.assets.uiskin );
            container.add(scrollPane).expand().fill().colspan(3);
            container.row().space(10).padBottom(10);

            final TextField   hostname    =   new TextField( game.hostname.getHostname() , skin );
            hostname.setMessageText( "<enter hostname>" );

            final TextField   port        =   new TextField( "8025" , skin );
            port.setMessageText( "<enter port>" );

            table.add( "hostname" ).right();
            table.add( hostname ).left();
            table.row();
            table.add( "port" ).right();
            table.add( port ).left();
            table.row();

            final TextButton startKryonetServerButton = new TextButton( "start kryonet" , skin);
            startKryonetServerButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            if( 0 == hostname.getText().length() || 0 == port.getText().length() )
                            {
                                return;
                            }

                            startKryonetServerButton.setDisabled(true);

                            Net.Server server = new cz.kor3k.snake.kryonet.Factory().createServer(hostname.getText(), port.getText());
                            server.create();

                            Gdx.app.log("ServerScreen" , "start server " + x + ", " + y);
                            game.setScreen(new ListenServerScreen( server ).create());
                        }
                    });

            final TextButton startWebsocketsServerButton = new TextButton( "start websockets" , skin);
            startWebsocketsServerButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            if( 0 == hostname.getText().length() || 0 == port.getText().length() )
                            {
                                return;
                            }

                            startWebsocketsServerButton.setDisabled(true);

                            Net.Server server = new cz.kor3k.snake.websockets.Factory().createServer( hostname.getText() , port.getText() );
                            server.create();

                            Gdx.app.log("ServerScreen" , "start server " + x + ", " + y);
                            game.setScreen(new ListenServerScreen( server ).create());
                        }
                    });

            container.add( createMainMenuButton() );
            container.add( startWebsocketsServerButton );
            container.add( startKryonetServerButton );

            return this;
        }
    }

    public static class ListenServerScreen extends ServerScreen
    {
        protected Net.Server    server;

        public ListenServerScreen( Net.Server server )
        {
            super();
            this.server            =   server;
        }

        @Override
        public void render( float delta )
        {
            table.clear();

            String[]    url = Net.parseUrl( server.getUrl().toString().substring(1) );
//            String[]    url = Net.parseUrl( server.getAddress().toString().substring( 1 ) );

            Label label =   this.createLabel( url[1] + " : " + url[2] , "white" );

            Label.LabelStyle ls =   new Label.LabelStyle( label.getStyle() );
            ls.font   =   game.assets.fontBold;
            label.setStyle( ls );

            table.add( label ).center().colspan( 3 );
            table.row();

            table.add( "client ip" );
            table.add( "client id" );
            table.add( "players" );
            table.row();


            //TODO: put into implementantions? factory?
            if( server instanceof cz.kor3k.snake.kryonet.Server )
            {
                for ( com.esotericsoftware.kryonet.Connection session : (List<com.esotericsoftware.kryonet.Connection>)server.getConnections() )
                {
                    if( !session.isConnected() )
                        continue;

                    table.add( session.getRemoteAddressTCP().getHostName() );
                    table.add( String.valueOf( session.hashCode() ) );

                }
            }
            else if( server instanceof cz.kor3k.snake.websockets.Server )
            {
                for ( org.java_websocket.WebSocket session : (List<org.java_websocket.WebSocket>)server.getConnections() )
                {
                    if( !session.isOpen() )
                        continue;

                    table.add( session.getRemoteSocketAddress().getHostName() );
                    table.add( String.valueOf( session.hashCode() ) );
                }
            }
            ////

            table.add( "" );
            table.row();

            super.render( delta );
        }

        @Override
        public Screen create()
        {
            final SnakeGame game =    this.game;

            this.prepareUi( game.assets.uiskin );
            container.add(scrollPane).expand().fill().colspan(2);
            container.row().space(10).padBottom(10);

            TextButton stopServerButton = new TextButton( "stop server" , skin);
            stopServerButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            server.dispose();

                            System.out.println("stop server " + x + ", " + y);
                            game.setScreen(new StartServerScreen().create());
                        }
                    });

            TextButton startGameButton = new TextButton( "start game" , skin);
            startGameButton.addListener(
                    new ClickListener()
                    {
                        public void clicked (InputEvent event, float x, float y)
                        {
                            server.sendStart();

                            System.out.println("start game " + x + ", " + y);
                            game.setScreen( new ServerWorldScreen( server ).create() );
                        }
                    });

            container.add( stopServerButton );
            container.add( startGameButton );

            return this;
        }
    }
}
