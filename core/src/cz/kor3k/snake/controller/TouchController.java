package cz.kor3k.snake.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.model.World;
import cz.kor3k.snake.view.MenuScreen;

/**
 * Created by martin on 13. 11. 2014.
 */
public class TouchController  extends InputAdapter
{
    private World world;
    private Game game;

    public TouchController(World world, Game game)
    {
        this.world  =   world;
        this.game   =   game;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button)
    {
        Gdx.app.debug("TouchController", "touchDown: " + "x: " + screenX + " y: " + screenY );

        if( this.world.isPaused() )
        {
            return new Paused().touchDown( screenX , screenY , pointer , button );
        }

        if( this.world.isRunning() )
        {
            return new Running().touchDown(screenX, screenY, pointer, button);
        }

        if( this.world.isReady() )
        {
            return new Ready().touchDown(screenX , screenY , pointer , button );
        }

        if( this.world.isOver() )
        {
            return new Over().touchDown(screenX, screenY, pointer, button);
        }

        return false;
    }

    public boolean keyDown(int keycode)
    {
        Gdx.app.debug("TouchController", "keyDown: " + Input.Keys.toString( keycode ) );

        if( this.world.isPaused() )
        {
            return new Paused().keyDown( keycode );
        }

        if( this.world.isRunning() )
        {
            return new Running().keyDown( keycode );
        }

        if( this.world.isReady() )
        {
            return new Ready().keyDown( keycode );
        }

        if( this.world.isOver() )
        {
            return new Over().keyDown( keycode );
        }

        return false;
    }

    private class Paused
    {
        public boolean touchDown(int screenX, int screenY, int pointer, int button)
        {
            Gdx.app.debug("TouchController", "resuming" );
            world.resume();
            return true;
        }

        public boolean keyDown(int keycode)
        {
            if( Input.Keys.BACK == keycode )
            {
                Gdx.app.debug("TouchController", "resuming" );
                world.resume();
                return true;
            }

            if( Input.Keys.HOME == keycode )
            {
                Gdx.app.debug("TouchController", "quitting" );
                game.setScreen( new MenuScreen().create() );
                return true;
            }

            //it is important to return true here, so the keyDown event is not passed to PlayerController.Keyboard instances
            //also, this must be the first InputProcessor in multiplexer
            return true;
        }
    }

    private class Ready
    {
        public boolean touchDown(int screenX, int screenY, int pointer, int button)
        {
            Gdx.app.debug("TouchController", "running" );
            world.setGameState( Game.State.RUNNING );
            return true;
        }

        public boolean keyDown(int keycode)
        {
            return true;
        }
    }

    private class Over
    {
        public boolean touchDown(int screenX, int screenY, int pointer, int button)
        {
            Gdx.app.debug("TouchController", "restarting");
            world.create();
            world.setGameState( Game.State.READY );
            return true;
        }

        public boolean keyDown(int keycode)
        {
            if( Input.Keys.HOME == keycode )
            {
                Gdx.app.debug("TouchController", "quitting");
                game.setScreen( new MenuScreen().create() );
                return true;
            }

            if ( Input.Keys.BACK == keycode )
            {
                Gdx.app.debug("TouchController", "restarting" );
                world.create();
                world.setGameState( Game.State.READY );
            }

            return true;
        }
    }

    private class Running
    {
        public boolean touchDown(int screenX, int screenY, int pointer, int button)
        {
            return false;
        }

        public boolean keyDown(int keycode)
        {
            if( Input.Keys.BACK == keycode )
            {
                Gdx.app.debug("TouchController", "pausing" );
                world.pause();
                return true;
            }

            return false;
        }
    }
}
