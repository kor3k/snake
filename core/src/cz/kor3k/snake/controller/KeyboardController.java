package cz.kor3k.snake.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.Screen;
import cz.kor3k.snake.SnakeGame;
import cz.kor3k.snake.model.World;
import cz.kor3k.snake.view.MenuScreen;

/**
 * Created by martin on 29. 10. 2014.
 */
public class KeyboardController extends InputAdapter
{
    private World   world;
    private Game    game;

    public KeyboardController(World world, Game game)
    {
        this.world  =   world;
        this.game   =   game;
    }

    public boolean keyDown(int keycode)
    {
        Gdx.app.debug("KeyboardController", "keyDown: " + Input.Keys.toString( keycode ) );

        if( this.world.isPaused() )
        {
            Gdx.app.debug("KeyboardController", "world is paused" );
            return new Paused().keyDown( keycode );
        }

        if( this.world.isRunning() )
        {
            Gdx.app.debug("KeyboardController", "world is running" );
            return new Running().keyDown( keycode );
        }

        if( this.world.isReady() )
        {
            Gdx.app.debug("KeyboardController", "world is ready" );
            return new Ready().keyDown( keycode );
        }

        if( this.world.isOver() )
        {
            Gdx.app.debug("KeyboardController", "world is over" );
            return new Over().keyDown( keycode );
        }

        return false;
    }

    private class Paused
    {
        public boolean keyDown(int keycode)
        {
            if( Input.Keys.P == keycode || Input.Keys.ESCAPE == keycode )
            {
                Gdx.app.debug("KeyboardController", "resuming" );
                world.resume();
                return true;
            }

            if( Input.Keys.Q == keycode || Input.Keys.E == keycode )
            {
                Gdx.app.debug("KeyboardController", "quitting" );
                game.setScreen( new MenuScreen().create() ) ;
                return true;
            }

            //it is important to return true here, so the keyDown event is not passed to PlayerController.Keyboard instances
            //also, this must be the first InputProcessor in multiplexer
            return true;
        }
    }

    private class Running
    {
        public boolean keyDown(int keycode)
        {
            if( Input.Keys.O == keycode )
            {
                Gdx.app.debug("KeyboardController", "game over" );
                world.setGameState( Game.State.OVER );
                return true;
            }

            if( Input.Keys.P == keycode || Input.Keys.ESCAPE == keycode )
            {
                Gdx.app.debug("KeyboardController", "pausing" );
                world.pause();
                return true;
            }

            if( Input.Keys.F1 == keycode )
            {
                Gdx.app.debug("KeyboardController", "toggle walls" );

                if( ((SnakeGame)game).settings.useWalls )
                {
                    ((SnakeGame)game).settings.useWalls =   false;
                }
                else
                {
                    ((SnakeGame)game).settings.useWalls =   true;
                }

                return true;
            }

            if( Input.Keys.F2 == keycode )
            {
                Gdx.app.debug("KeyboardController", "toggle wormholes" );

                if( ((SnakeGame)game).settings.useWormholes )
                {
                    ((SnakeGame)game).settings.useWormholes =   false;
                }
                else
                {
                    ((SnakeGame)game).settings.useWormholes =   true;
                }

                return true;
            }

            return false;
        }
    }

    private class Ready
    {
        public boolean keyDown(int keycode)
        {
            if( Input.Keys.ENTER == keycode )
            {
                Gdx.app.debug("KeyboardController", "running" );
                world.setGameState( Game.State.RUNNING );
                return true;
            }

            return true;
        }
    }

    private class Over
    {
        public boolean keyDown(int keycode)
        {
            if( Input.Keys.ESCAPE == keycode || Input.Keys.Q == keycode || Input.Keys.E == keycode )
            {
                Gdx.app.debug("KeyboardController", "quitting" );
                game.setScreen( new MenuScreen().create() );
                return true;
            }

            if ( Input.Keys.ENTER == keycode || Input.Keys.R == keycode )
            {
                world.create();
                world.setGameState(Game.State.READY);
                return true;
            }

            return true;
        }
    }
}
