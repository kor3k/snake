package cz.kor3k.snake.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import cz.kor3k.forge.Game;
import cz.kor3k.snake.Net;
import cz.kor3k.snake.SnakeGame;
import cz.kor3k.snake.model.Player;
import cz.kor3k.snake.model.Snake;

/**
 * Created by martin on 29. 10. 2014.
 */
public abstract class PlayerController extends InputAdapter
{
    protected Player    player;
    protected transient SnakeGame game    =   (SnakeGame) Game.obj;

    public PlayerController( Player player )
    {
        this.setPlayer( player );
    }

    public PlayerController()
    {

    }

    public Player getPlayer()
    {
        return this.player;
    }

    public void setPlayer( Player player )
    {
        this.player =  player;
    }

    public void turnLeft()
    {
        if( !this.canTurn() )
            return;

        if( this.player.getSnake().new Buffs().hasConfuse() )
        {
            this.doTurnRight();
        }
        else
        {
            this.doTurnLeft();
        }
    }

    public void turnRight()
    {
        if( !this.canTurn() )
            return;

        if( this.player.getSnake().new Buffs().hasConfuse() )
        {
            this.doTurnLeft();
        }
        else
        {
            this.doTurnRight();
        }
    }

    protected void doTurnLeft()
    {
        Gdx.app.debug("snake.controller.PlayerController", "turning left player" + this.player.toString()  );
        this.player.getSnake().turnLeft();
    }

    protected void doTurnRight()
    {
        Gdx.app.debug("snake.controller.PlayerController", "turning right player" + this.player.toString()  );
        this.player.getSnake().turnRight();
    }

    protected boolean canTurn()
    {
        //TODO: new Powerup/Buff : lose of control. return here if buff present.
        Snake.Buffs buffs   =   this.player.getSnake().new Buffs();
        if( 0 == this.player.getSnake().getSpeed() || buffs.hasFreeze() )
            return false;

        return true;
    }

    public static class Touch extends PlayerController
    {
        private Rectangle turnLeftArea;
        private Rectangle turnRightArea;

        public Touch(Player player, Rectangle turnLeftArea, Rectangle turnRightArea )
        {
            super(player);
            this.turnLeftArea    =   turnLeftArea;
            this.turnRightArea   =   turnRightArea;
        }

        public boolean touchDown(int screenX, int screenY, int pointer, int button)
        {
            Vector2 touchPos    =   new Vector2( screenX , this.game.getScreenHeight() - screenY );

            Gdx.app.debug("snake.controller.PlayerController.Touch", "touch with x: " + screenX + " and y: " + screenY  );
            Gdx.app.debug("snake.controller.PlayerController.Touch", "touchPos x: " + touchPos.x + " and y: " + touchPos.y  );

            if( this.turnLeftArea.contains( touchPos ) ) // if touched inside player's turnLeftArea
            {
                this.turnLeft();
                return true;
            }
            else if( this.turnRightArea.contains( touchPos ) ) // if touched inside player's turnRightArea
            {
                this.turnRight();
                return true;
            }

            return false;
        }
    }

    public static class Keyboard extends PlayerController
    {
        private int turnLeftKey;
        private int turnRightKey;

        public Keyboard(Player player, int turnLeftKey, int turnRightKey)
        {
            super(player);
            this.turnLeftKey    =   turnLeftKey;
            this.turnRightKey   =   turnRightKey;
        }

        public boolean keyDown(int keycode)
        {
            if( this.turnLeftKey == keycode )
            {
                this.turnLeft();
                return true;
            }
            else if( this.turnRightKey == keycode )
            {
                this.turnRight();
                return true;
            }

            return false;
        }
    }

    public static class Network
    {
        public static class Server extends PlayerController
        {
            protected Net.Server server;

            public Server( Player player , Net.Server server )
            {
                super( player );
                this.server =   server;
            }
        }

        public static class Client
        {
            protected Net.Client client;

            public Client( Net.Client client )
            {
                this.client =   client;
            }

            private void doAction( Player player , int action )
            {
                client.send(new Net.Message(
                        Net.Message.MessageType.CONTROLLER,
                        new Message( player.getId() , action )
                ));
            }

            public class Keyboard extends PlayerController.Keyboard
            {

                public Keyboard(Player player, int turnLeftKey, int turnRightKey)
                {
                    super(player, turnLeftKey, turnRightKey);
                }

                @Override
                protected void doTurnLeft()
                {
                    Gdx.app.debug("websockets.PlayerController.Keyboard", "sending player LEFT turn to server" + this.getPlayer().toString());
                    doAction( player , Network.Message.TURN_LEFT );
                }

                @Override
                protected void doTurnRight()
                {
                    Gdx.app.debug("websockets.PlayerController.Keyboard", "sending player RIGHT turn to server" + this.getPlayer().toString());
                    doAction( player , Network.Message.TURN_RIGHT );
                }
            }

            public class Touch extends PlayerController.Touch
            {

                public Touch(Player player, Rectangle turnLeftArea, Rectangle turnRightArea)
                {
                    super(player, turnLeftArea, turnRightArea);
                }

                @Override
                protected void doTurnLeft()
                {
                    Gdx.app.debug("websockets.PlayerController.Touch", "sending player LEFT turn to server" + this.getPlayer().toString());
                    doAction(player, Network.Message.TURN_LEFT);
                }

                @Override
                protected void doTurnRight()
                {
                    Gdx.app.debug("websockets.PlayerController.Touch", "sending player RIGHT turn to server" + this.getPlayer().toString());
                    doAction(player, Network.Message.TURN_RIGHT);
                }
            }
        }

        public static class Message
        {
            public static final int TURN_LEFT   =   0;
            public static final int TURN_RIGHT  =   1;

            public String playerId;
            public int    action;

            public Message()
            {

            }

            public Message( String playerId , int action )
            {
                this.playerId   =   playerId;
                this.action     =   action;
            }
        }
    }
}
