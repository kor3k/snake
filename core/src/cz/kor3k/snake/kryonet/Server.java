package cz.kor3k.snake.kryonet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.minlog.Log;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.KryoNet;
import cz.kor3k.snake.Net;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import cz.kor3k.snake.Settings;
import cz.kor3k.snake.SnakeGame;

/**
 * Created by martin on 7. 12. 2014.
 */
public class Server extends KryoNet.Server implements Net.Server
{

    private SnakeGame           game            =   (SnakeGame) Game.obj;
    private Net.ServerHandler    handler =   new Net.ServerHandler();


    public Server(String hostname, int tcpPort, int udpPort) throws IOException
    {
        super(hostname, tcpPort, udpPort);
        Log.DEBUG();
    }

    @Override
    public Thread create()
    {
        return super.create();
    }

    @Override
    protected com.esotericsoftware.kryonet.Server createKryoServer()
    {
        com.esotericsoftware.kryonet.Server server  =   super.createKryoServer( 24576 , 8192 ); //16384 , 2048

        Utils.registerClasses(server.getKryo());

        return server;
    }

    @Override
    public void disconnected(Connection connection)
    {
        super.disconnected( connection );
    }

    @Override
    public void connected(Connection connection)
    {
        super.connected( connection );
    }

    @Override
    public void received(Connection conn, Object object)
    {
        super.received( conn , object );

        Net.Message  netMessage;
        if( object instanceof String )
        {
            netMessage   =   Net.Message.fromJson( (String)object );
        }
        else if( object instanceof Net.Message )
        {
            netMessage   =   (Net.Message)object;
        }
        else
        {
            Gdx.app.log( "KryoServer" , "received this: " + object.getClass() + " : " + object.toString() );
            return;
        }

        if( Net.Message.MessageType.CONNECT == netMessage.getType() )
        {
            this.onConnectMessage(conn, netMessage);

            Gdx.app.log( "Server" , "yep, CONNECT" );
        }
        else if( Net.Message.MessageType.CONTROLLER == netMessage.getType() )
        {
            this.onControllerMessage( conn , netMessage );

            Gdx.app.log( "Server" , "yep, CONTROLLER" );
        }
        else if( Net.Message.MessageType.CHAT == netMessage.getType() )
        {
            this.onChatMessage( conn , netMessage );

            Gdx.app.log( "Server" , "yep, CHAT" );
        }
    }

    @Override
    public void idle(Connection connection)
    {
        super.idle( connection );
    }

    @Override
    public void broadcast(String message)
    {
        super.broadcast( message );
    }

    @Override
    public void broadcast(Net.Message message)
    {
        super.broadcast( message );
//        this.broadcast( Net.Message.toJson( message ) );
    }

    @Override
    public void dispose()
    {
        super.dispose();
    }

    @Override
    public URI getUrl()
    {
        return super.getUrl();
    }

    @Override
    public void broadcast(cz.kor3k.forge.World world)
    {
        Net.Message message =   handler.getWorldMessage();
        message.setPayload( world );
        this.broadcast(message);
    }


    public void sendStart()
    {
        Net.Message message =   handler.getStartMessage();
        Gdx.app.log( "Server" , new Json().toJson( game.settings ) );
        this.broadcast(message);
    }

    public List<Connection> getConnections()
    {
        return super.getConnections();
    }

    protected void onChatMessage(Connection conn, Net.Message message)
    {
        handler.onChatMessage( message );
    }

    protected void onControllerMessage(Connection conn, Net.Message message)
    {
        handler.onControllerMessage( message );

        //find player's controller and move
    }

    protected void onConnectMessage(Connection conn, Net.Message message)
    {
        Settings settings    =   (Settings)message.getPayload();
        Gdx.app.log( "Server" , "settings: " + settings );

        //add player (settings), but use NetworkController

        handler.onConnectMessage( message );
    }
}
