package cz.kor3k.snake.kryonet;

import com.esotericsoftware.kryo.Kryo;
import cz.kor3k.snake.Settings;

/**
 * Created by martin on 12. 12. 2014.
 */
public class Utils
{
    public static void registerClasses( Kryo kryo )
    {
        kryo.register(Settings.class);
        kryo.register(Settings.Player.class);
        kryo.register(java.util.ArrayList.class);
        kryo.register(com.badlogic.gdx.math.Rectangle.class);
        kryo.register(com.badlogic.gdx.math.Circle.class);
        kryo.register(cz.kor3k.snake.model.World.class);
        kryo.register(cz.kor3k.forge.Game.State.class);
        kryo.register(cz.kor3k.snake.model.Player.class);
        kryo.register(cz.kor3k.snake.model.Snake.class);
        kryo.register(cz.kor3k.snake.model.Snake.Buff.class);
        kryo.register(cz.kor3k.snake.model.Snake.Buff.Confuse.class);
        kryo.register(cz.kor3k.snake.model.Snake.Buff.Freeze.class);
        kryo.register(cz.kor3k.snake.model.Snake.Buff.Noclip.class);
        kryo.register(cz.kor3k.snake.model.Snake.Buff.Speed.class);
        kryo.register(cz.kor3k.snake.model.Food.class);
        kryo.register(cz.kor3k.snake.model.Powerup.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Blockade.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Chop.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Confuse.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Fast.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Freeze.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Noclip.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Reverse.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Shrink.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Slow.class);
        kryo.register(cz.kor3k.snake.model.Powerup.SwapPlayers.class);
        kryo.register(cz.kor3k.snake.model.Powerup.SwapSnakes.class);
        kryo.register(cz.kor3k.snake.model.Powerup.Tunnel.class);
        kryo.register(cz.kor3k.snake.model.Joint.class);
        kryo.register(cz.kor3k.snake.model.Block.class);
        kryo.register(cz.kor3k.snake.model.Wall.class);
        kryo.register(cz.kor3k.snake.model.Wormhole.class);
        kryo.register(com.badlogic.gdx.math.Vector2.class);
        kryo.register(cz.kor3k.forge.Object.class);
        kryo.register(cz.kor3k.snake.controller.PlayerController.Network.Message.class);
        kryo.register(com.badlogic.gdx.graphics.Color.class);
    }
}
