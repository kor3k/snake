package cz.kor3k.snake.kryonet;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.io.IOException;

/**
 * Created by martin on 7. 12. 2014.
 */
public class ClientTest
{
    public static void main(String[] args) throws IOException
    {
        Client client = new Client();
        client.start();
        client.connect(5000, "127.0.0.1", 54555, 54777);

        Kryo kryo = client.getKryo();
        kryo.register(ServerTest.SomeRequest.class);
        kryo.register(ServerTest.SomeResponse.class);

        ServerTest.SomeRequest request = new ServerTest.SomeRequest();
        request.text = "Here is the request";
        client.sendTCP(request);
        client.sendUDP(request);

        client.addListener(new Listener()
        {
            public void received (Connection connection, Object object)
            {
                if (object instanceof ServerTest.SomeResponse) {
                    ServerTest.SomeResponse response = (ServerTest.SomeResponse)object;
                    System.out.println(response.text);
                }
            }
        });

        while(true){}
    }
}
