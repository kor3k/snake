package cz.kor3k.snake.kryonet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.minlog.Log;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.KryoNet;
import cz.kor3k.snake.Net;
import cz.kor3k.snake.SnakeGame;

import java.io.IOException;
import java.net.URI;

/**
 * Created by martin on 7. 12. 2014.
 */
public class Client extends KryoNet.Client implements Net.Client
{
    private SnakeGame game            =   (SnakeGame) Game.obj;
    private Net.ClientHandler    handler =   new Net.ClientHandler();

    public Client(int timeout, String host, int tcpPort, int udpPort) throws IOException
    {
        super(timeout, host, tcpPort, udpPort);
        Log.DEBUG();
    }

    @Override
    public Thread create()
    {
        return super.create();
    }

    @Override
    protected com.esotericsoftware.kryonet.Client createKryoClient()
    {
        com.esotericsoftware.kryonet.Client client  =   super.createKryoClient( 16384, 8192 );

        Utils.registerClasses(client.getKryo());

        return client;
    }

    @Override
    public void connected(Connection connection)
    {
        super.connected( connection );
        this.sendSettings();
    }

    @Override
    public void disconnected(Connection connection)
    {
        handler.onClose();
        super.disconnected( connection );
    }

    @Override
    public void received(Connection conn, Object object)
    {
        super.received( conn , object );

        Net.Message  netMessage;
        if( object instanceof String )
        {
            netMessage   =   Net.Message.fromJson( (String)object );
        }
        else if( object instanceof Net.Message )
        {
            netMessage   =   (Net.Message)object;
        }
        else
        {
            Gdx.app.log( "KryoClient" , "received this: " + object.getClass() + " : " + object.toString() );
            return;
        }

        try
        {
            if( Net.Message.MessageType.WORLD == netMessage.getType() )
            {
                this.onWorldMessage( conn , netMessage );
            }
            else if( Net.Message.MessageType.START == netMessage.getType() )
            {
                this.onStartMessage (conn , netMessage );
            }
            else if( Net.Message.MessageType.CHAT == netMessage.getType() )
            {
                this.onChatMessage( conn , netMessage );
            }
        }
        catch (com.badlogic.gdx.utils.SerializationException e)
        {
            e.printStackTrace();
            Gdx.app.log("error", e.getMessage() + " " + e.getCause() + " " + e.getSuppressed().toString() );
        }
    }

    @Override
    public void idle(Connection connection)
    {
        super.idle( connection );
    }

    @Override
    public void send(String message)
    {
        super.send( message );
    }

    @Override
    public void send(Net.Message message)
    {
        super.send( message );
//        this.send( Net.Message.toJson(message) );
    }

    @Override
    public void dispose()
    {
        super.dispose();
    }

    @Override
    public URI getUrl()
    {
        return super.getUrl();
    }


    public void sendSettings()
    {
        Net.Message message =   handler.getConnectMessage();
        Gdx.app.debug("Client", new Json().toJson( message));
        this.send( message );
    }

    protected void onWorldMessage(Connection conn, Net.Message message)
    {
        handler.onWorldMessage( message );

        Gdx.app.debug( "Client" , "remoteWorld: " + game.remoteWorld );
    }

    protected void onChatMessage(Connection conn, Net.Message message)
    {
        handler.onChatMessage(message);

        Gdx.app.log( "Client" , "yep, CHAT" );
    }

    protected void onStartMessage(Connection conn, Net.Message message)
    {
        handler.onStartMessage( message );

        Gdx.app.log( "Client" , "settings: " + game.serverSettings );
    }
}
