package cz.kor3k.snake.kryonet;

import cz.kor3k.snake.Net;

import java.io.IOException;

/**
 * Created by martin on 12. 12. 2014.
 */
public class Factory implements Net.Factory
{
    public Net.Client createClient( String host , String port )
    {
        try
        {
            return new Client( 5000 , host , Integer.valueOf( port ) , Integer.valueOf( port ) + 1 );
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public Net.Server createServer( String host , String port )
    {
        try
        {
            return new Server( host , Integer.valueOf( port ) , Integer.valueOf( port ) + 1 );
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
