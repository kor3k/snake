package cz.kor3k.snake;

import com.badlogic.gdx.Gdx;
import cz.kor3k.forge.Game;
import cz.kor3k.snake.controller.PlayerController;
import cz.kor3k.snake.model.World;

/**
 * Created by martin on 8. 12. 2014.
 */
public class Net extends cz.kor3k.forge.Net
{
    public static interface Server extends cz.kor3k.forge.Net.WorldServer
    {
        void sendStart();
    }

    public static interface Client extends cz.kor3k.forge.Net.Client
    {
        void sendSettings();
    }


    public static class ServerHandler
    {
        protected SnakeGame game  =   (SnakeGame) Game.obj;

        public Message getStartMessage()
        {
            Message message =   new Message();
            message.setType( Message.MessageType.START );
            message.setPayload( game.settings );

            return message;
        }

        public Message getWorldMessage()
        {
            Message message =   new Message();
            message.setType(Message.MessageType.WORLD);

            return message;
        }


        public void onMessage( String message )
        {
            Message  netMessage   =  Message.fromJson( message );

            if( Message.MessageType.CONNECT == netMessage.getType() )
            {
                this.onConnectMessage(netMessage);

                Gdx.app.log( "Server" , "yep, CONNECT" );
            }
            else if( Message.MessageType.CONTROLLER == netMessage.getType() )
            {
                this.onControllerMessage(netMessage);

                Gdx.app.log( "Server" , "yep, CONTROLLER" );
            }
            else if( Message.MessageType.CHAT == netMessage.getType() )
            {
                this.onChatMessage( netMessage );

                Gdx.app.log( "Server" , "yep, CHAT" );
            }
        }

        public void onChatMessage( Message message )
        {

        }

        public void onConnectMessage(Message message)
        {
            Settings    settings    =   (Settings)message.getPayload();
            Gdx.app.log( "Server" , "settings: " + settings );

            //add player (settings), but use NetworkController

            for( Settings.Player p : settings.getPlayers() )
            {
                Settings.Player player  =   new Settings.Player( p );

                player.isNet    =   true;

                game.settings.addPlayer( player );
            }
        }

        public void onControllerMessage(Message message)
        {
            PlayerController.Network.Message action    =   (PlayerController.Network.Message)message.getPayload();
            Gdx.app.log( "Server" , "controller action: " + action.action );

            PlayerController ctrl   =   this.getPlayerController( action.playerId );

            if( action.action == PlayerController.Network.Message.TURN_LEFT )
                ctrl.turnLeft();
            else if( action.action == PlayerController.Network.Message.TURN_RIGHT )
                ctrl.turnRight();
            else
                Gdx.app.log( "Server.onControllerMessage" , "unknown action: " + action.action );

            //find player's controller and move
        }

        private PlayerController getPlayerController( String playerId )
        {
            for( PlayerController ctrl : game.playerControllers )
            {
                if (playerId.equals( ctrl.getPlayer().getId() ) )
                {
                    return ctrl;
                }
            }

            Gdx.app.log( "Server" , "controller not found for playerId: " + playerId );
            return null;
        }
    }

    public static class ClientHandler
    {
        protected SnakeGame game  =   (SnakeGame) Game.obj;

        public Message getConnectMessage()
        {
            Message message =   new Message();
            message.setType( Message.MessageType.CONNECT );
            message.setPayload( game.settings );

            return message;
        }

        public void onMessage(String message)
        {
            Message  netMessage   =   Message.fromJson( message );

            try
            {
                if( Message.MessageType.WORLD == netMessage.getType() )
                {
                    this.onWorldMessage( netMessage );
                }
                else if( Message.MessageType.START == netMessage.getType() )
                {
                    this.onStartMessage( netMessage);
                }
                else if( Message.MessageType.CHAT == netMessage.getType() )
                {
                    //chat message to chat object
                    this.onChatMessage( netMessage );
                }
            }
            catch (com.badlogic.gdx.utils.SerializationException e)
            {
                e.printStackTrace();
                Gdx.app.log("error", e.getMessage() + " " + e.getCause() + " " + e.getSuppressed().toString() );
            }
        }

        public void onStartMessage(Message message)
        {
            game.serverSettings    =   (Settings)message.getPayload();
            game.flagStart  =   true;
        }

        public void onWorldMessage(Message message)
        {
            game.remoteWorld    =   (World)message.getPayload();
        }

        public void onChatMessage( Message message )
        {
            
        }

        public void onClose()
        {
            game.flagExit   =   true;
        }

        public void onError( Exception ex )
        {
            game.flagExit   =   true;
        }
    }
}
