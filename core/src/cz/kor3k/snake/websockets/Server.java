package cz.kor3k.snake.websockets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.WebSockets;
import cz.kor3k.snake.Settings;
import cz.kor3k.snake.SnakeGame;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.handshake.ClientHandshake;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

import cz.kor3k.snake.Net;


/**
 * Created by martin on 1. 12. 2014.
 */
public class Server extends WebSockets.Server implements Net.Server
{
    private SnakeGame game  =   (SnakeGame) Game.obj;
    private Net.ServerHandler    handler =   new Net.ServerHandler();


    public Server(InetSocketAddress address )
    {
        super(address);
    }

    public Server(String hostname, int port) throws UnknownHostException
    {
        super(hostname, port);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake)
    {
        super.onOpen(conn,handshake);
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote)
    {
        super.onClose(conn,code,reason,remote);
    }

    @Override
    public void onMessage(WebSocket conn, String message)
    {
        super.onMessage(conn,message);

        Net.Message  wsMessage   =   Net.Message.fromJson( message );

        if( Net.Message.MessageType.CONNECT == wsMessage.getType() )
        {
            this.onConnectMessage(conn, wsMessage);

            Gdx.app.log( "Server" , "yep, CONNECT" );
        }
        else if( Net.Message.MessageType.CONTROLLER == wsMessage.getType() )
        {
            this.onControllerMessage( conn , wsMessage );

            Gdx.app.log( "Server" , "yep, CONTROLLER" );
        }
        else if( Net.Message.MessageType.CHAT == wsMessage.getType() )
        {
            this.onChatMessage( conn , wsMessage );

            Gdx.app.log( "Server" , "yep, CHAT" );
        }
    }


    protected void onChatMessage(WebSocket conn, Net.Message message)
    {
        handler.onChatMessage( message );
    }

    protected void onControllerMessage(WebSocket conn, Net.Message message)
    {
        handler.onControllerMessage( message );

        //find player's controller and move
    }

    protected void onConnectMessage(WebSocket conn, Net.Message message)
    {
        Settings    settings    =   (Settings)message.getPayload();
        Gdx.app.log( "Server" , "settings: " + settings );

        //add player (settings), but use NetworkController

        handler.onConnectMessage( message );
    }

    @Override
    public void onError(WebSocket conn, Exception ex)
    {
        super.onError(conn,ex);
    }

    @Override
    public void broadcast(cz.kor3k.forge.World world)
    {
        Net.Message message =   handler.getWorldMessage();
        message.setPayload( world );
        this.broadcast(message);
    }


    public void sendStart()
    {
        Net.Message message =   handler.getStartMessage();

        Gdx.app.debug("Server", new Json().toJson(message));

        this.broadcast(message);
    }

    public static void main( String[] args ) throws InterruptedException , IOException {
        WebSocketImpl.DEBUG = true;
        int port = 8887; // 843 flash policy port
        try {
            port = Integer.parseInt( args[ 0 ] );
        } catch ( Exception ex ) {
        }

        String boundHostname    =   "";
        try
        {
            boundHostname    =   InetAddress.getLocalHost().getCanonicalHostName();
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
            boundHostname   =   "localhost";
        }

        Server  server      =   new Server( boundHostname , port );
//        Server  server      =   new Server( new InetSocketAddress( port ) );
        server.create();

        Gdx.app.log( "Server" , "Server started on: " + server.getAddress().getHostName() + ":" + server.getPort() );

        BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
        while ( true ) {
            String in = sysin.readLine();
            server.broadcast(in);
            if( in.equals( "exit" ) ) {
                server.stop();
                break;
            }
        }
    }
}
