package cz.kor3k.snake.websockets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.WebSockets;
import cz.kor3k.snake.SnakeGame;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.handshake.ServerHandshake;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.NotYetConnectedException;

import cz.kor3k.snake.Net;

/**
 * Created by martin on 1. 12. 2014.
 */
public class Client extends WebSockets.Client implements Net.Client
{
    private SnakeGame   game  =   (SnakeGame) Game.obj;
    private Net.ClientHandler    handler =   new Net.ClientHandler();


    public Client( URI serverURI )
    {
        super( serverURI );
    }

    public Client( String hostname, int port ) throws URISyntaxException
    {
        super( hostname , port );
    }

    @Override
    public void onOpen(ServerHandshake handshakedata)
    {
        super.onOpen(handshakedata);
        this.sendSettings();
    }

    public void sendSettings()
    {
        Net.Message message =   handler.getConnectMessage();
        Gdx.app.debug( "Client" , new Json().toJson( message ) );
        this.send( message );
    }

    @Override
    public void onMessage(String message)
    {
//        super.onMessage(message);

        Net.Message  wsMessage   =   Net.Message.fromJson( message );

        try
        {
            if( Net.Message.MessageType.WORLD == wsMessage.getType() )
            {
                this.onWorldMessage( this.getConnection() , wsMessage );
            }
            else if( Net.Message.MessageType.START == wsMessage.getType() )
            {
                this.onStartMessage(this.getConnection(), wsMessage);
            }
            else if( Net.Message.MessageType.CHAT == wsMessage.getType() )
            {
                this.onChatMessage( this.getConnection() , wsMessage );
            }
        }
        catch (com.badlogic.gdx.utils.SerializationException e)
        {
            e.printStackTrace();
            Gdx.app.log("error", e.getMessage() + " " + e.getCause() + " " + e.getSuppressed().toString() );
        }
    }

    protected void onChatMessage(WebSocket conn, Net.Message message)
    {
        handler.onChatMessage(message);

        Gdx.app.log( "Client" , "yep, CHAT" );
    }

    protected void onStartMessage(WebSocket conn, Net.Message message)
    {
        handler.onStartMessage( message );

        Gdx.app.log( "Client" , "settings: " + game.serverSettings );
    }

    protected void onWorldMessage(WebSocket conn, Net.Message message)
    {
        handler.onWorldMessage( message );

        Gdx.app.debug( "Client" , "remoteWorld: " + game.remoteWorld );
    }

    @Override
    public void onClose(int code, String reason, boolean remote)
    {
        super.onClose(code, reason, remote);
        handler.onClose();
    }

    @Override
    public void onError(Exception ex)
    {
        super.onError(ex);
        handler.onError( ex );
    }

    @Override
    public void send(String text) throws NotYetConnectedException
    {
        super.send(text);
    }

    public static void main(String[] args) throws IOException
    {
        WebSocketImpl.DEBUG = true;
        String location =   "ws://172.16.120.250:8887";

        Client  client   =   new Client( URI.create( location ) );
        client.create();

        BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
        while ( true )
        {
            String in = sysin.readLine();

            client.send( in );

            if( in.equals( "exit" ) )
            {
                System.exit(1);
            }
        }
    }
}
