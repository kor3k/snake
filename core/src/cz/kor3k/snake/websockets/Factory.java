package cz.kor3k.snake.websockets;

import cz.kor3k.snake.Net;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by martin on 12. 12. 2014.
 */
public class Factory implements Net.Factory
{
    public Net.Client createClient( String host , String port )
    {
        try
        {
            return new Client( host , Integer.valueOf( port ) );
        }
         catch (URISyntaxException e)
         {
            e.printStackTrace();
            return null;
        }
    }

    public Net.Server createServer( String host , String port )
    {
        try
        {
            return new Server( host , Integer.valueOf( port ) );
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
