package cz.kor3k.snake;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Rectangle;
import cz.kor3k.forge.Game;
import cz.kor3k.forge.Hostname;
import cz.kor3k.snake.controller.PlayerController;
import cz.kor3k.snake.model.World;
import cz.kor3k.snake.view.StartScreen;

import java.util.ArrayList;
import java.util.List;


public class SnakeGame extends Game
{
    public Assets   assets;
    public Settings settings;
    public List<PlayerController> playerControllers   =   new ArrayList<>();

    public volatile boolean  flagStart;
    public volatile boolean  flagExit;
    public volatile World remoteWorld;
    public  Settings    serverSettings;

    public final Hostname hostname;

    //TODO: passing through wormhole adds score
    //TODO: different score for each powerup type
    //TODO: food with more calories

    public SnakeGame( Hostname hostname )
    {
        super();
        this.settings   =   this.initSettings();
        this.hostname   =   hostname;
    }

    public void create()
    {
        super.create();

        Gdx.app.setLogLevel( Application.LOG_INFO );
        this.assets     =   new Assets();
        this.assets.initColors();
        this.assets.create();
        this.font       =   assets.fontRegular;

        //http://code.google.com/p/kryonet/source/browse/trunk/kryonet/examples/com/esotericsoftware/kryonet/examples/chat/ChatServer.java
        //https://github.com/EsotericSoftware/kryonet
        //https://github.com/TooTallNate/Java-WebSocket/blob/master/src/main/example/ChatServer.java
        //https://github.com/pepedeab/libGDX-Net
        //http://java-websocket.org/

        //tween engine

        this.setScreen( new StartScreen( this ).create() );
    }

    public void render()
    {
        super.render();

        Gdx.graphics.setTitle( this.settings.windowTitle + " @ " + Integer.toString( Gdx.graphics.getFramesPerSecond() ) + " fps" );
    }

    public void dispose()
    {
        super.dispose();
        this.assets.dispose();
    }

    public void resize(int width, int height)

    {
        super.resize(width, height);
        this.resizeTouchAreas(width,height);
    }

    private void resizeTouchAreas(int width, int height)
    {
        Settings.Player p0  =   settings.getPlayer(0);

        p0.leftArea.set(
                0 ,
                ( height / 2 ) - 1 ,
                ( width / 2 ) - 1 ,
                ( height / 2 ) - 1
        );

        p0.rightArea.set(
                0 ,
                0 ,
                ( width / 2 ) - 1 ,
                ( height / 2 ) - 1
        );

        try
        {
            Settings.Player p1  =   settings.getPlayer(1);

            p1.leftArea.set(
                    ( width / 2 ) + 1 ,
                    0 ,
                    ( width / 2 ) - 1 ,
                    ( height / 2 ) - 1
            );

            p1.rightArea.set(
                    ( width / 2 ) + 1 ,
                    ( height / 2 ) + 1 ,
                    ( width / 2 ) - 1 ,
                    ( height / 2 ) - 1
            );
        }
        catch( IndexOutOfBoundsException e )
        {

        }
    }

    private Settings initSettings()
    {
        Settings settings   =   new Settings();

        Settings.Player p1  =   settings.addPlayer();
        p1.leftKey      =   Input.Keys.LEFT;
        p1.rightKey     =   Input.Keys.RIGHT;
        p1.leftArea     =   new Rectangle();
        p1.rightArea    =   new Rectangle();

        Settings.Player p2  =   settings.addPlayer();
        p2.leftKey      =   Input.Keys.J;
        p2.rightKey     =   Input.Keys.K;
        p2.leftArea     =   new Rectangle();
        p2.rightArea    =   new Rectangle();

        return settings;
    }

//        try {
//            Thread.sleep(1000);                 //1000 milliseconds is one second.
//        } catch(InterruptedException ex) {
//            Thread.currentThread().interrupt();
//        }
}