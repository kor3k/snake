package cz.kor3k.snake;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.*;

/**
 * Created by martin on 2. 11. 2014.
 */
public class Settings
{
    private List<Player> players    =   new ArrayList<>();
    public boolean  useWalls        =   false;
    public boolean  useWormholes    =   false;
    public final float  gameSpeed       =   0.05f;
    public final float  minSnakeSpeed   =   0.25f;
    public final float  maxSnakeSpeed   =   0.15f;
    public String   windowTitle     =   "Snake";
    

    public Settings( Settings toCopy )
    {
        for( Player p : toCopy.getPlayers() )
        {
            Player np    =   this.addPlayer();
            np.name      =   p.name;
            np.color     =   p.color;
            np.leftArea  =   p.leftArea;
            np.rightArea =   p.rightArea;
            np.leftKey   =   p.leftKey;
            np.rightKey  =   p.rightKey;
        }
    }

    public Settings()
    {

    }

    public Player addPlayer()
    {
        return this.addPlayer( "Player " + ( this.players.size() + 1) );
    }

    public Player addPlayer( String name )
    {
        Player p    =   new Player();
        p.name      =   name;
        this.players.add(p);
        return p;
    }

    public Player addPlayer( Player player )
    {
        this.players.add(player);
        return player;
    }

    public List<Player> getPlayers()
    {
        return this.players;
    }

    public Player getPlayer( String id )
    {
        for( Player p : players )
        {
            if( p.id.equals( id ) )
                return p;
        }

        throw new RuntimeException( "invalid player id" );
    }

    public Player getPlayer( int index )
    {
        return players.get(index);
    }

    public static String toJson( Settings settings )
    {
        return new Json().toJson( settings );
    }

    public static Settings fromJson( String json )
    {
        return new Json().fromJson( Settings.class , json );
    }

    public static class Player
    {
        public Integer leftKey;
        public Integer rightKey;
        public Rectangle leftArea;
        public Rectangle rightArea;
        public String name;
        public Color color;
        public String id;

        public boolean  isNet    =   false;

        private static SecureRandom random = new SecureRandom();

        public Player()
        {
            this.id     =   generateId();
            this.color  =   randomColor();
        }

        private Color randomColor()
        {
            Random  rnd =   new Random();
            return new Color( rnd.nextFloat() , rnd.nextFloat() , rnd.nextFloat() , 1.0f );
        }

        public Player( Player toCopy )
        {
            this.id     =   toCopy.id;
            this.name   =   toCopy.name;
            this.color  =   toCopy.color;
        }

        public static String generateId()
        {
            return new BigInteger(130, random).toString(32);
        }
    }
}
