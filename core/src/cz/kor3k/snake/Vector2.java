package cz.kor3k.snake;

/**
 * Created by martin on 6. 11. 2014.
 */
public class Vector2 extends com.badlogic.gdx.math.Vector2
{
    private int direction;

    public Vector2(com.badlogic.gdx.math.Vector2 position)
    {
        super(position);
    }

    public int getDirection()
    {
        return this.direction;
    }

    public Vector2 setDirection(int direction)
    {
        this.direction = direction;
        return this;
    }
}
