package cz.kor3k.snake;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.HashMap;

/**
 * Created by martin on 29. 10. 2014.
 */
public class Assets extends cz.kor3k.forge.Assets
{
    public Texture  joint;
    public Texture  food;
    public Texture  powerup;
    public Texture  wall;
    public Texture  logo;

    public FileHandle   fontRegularFile;
    public FileHandle   fontBoldFile;

    public BitmapFont   fontRegular;
    public BitmapFont   fontBold;
    public BitmapFont   fontBig;
    public BitmapFont   fontSmall;

    public Skin         uiskin;

    public final HashMap<String,Color> colors  =   new HashMap<>();

    public Assets initColors()
    {
        this.colors.put( "white" , new Color( 1.0f , 1.0f , 1.0f , 1.0f ) );
        this.colors.put( "yellow-green" , new Color(0.5f, 0.5f, 0, 1));
        this.colors.put( "dark-green" ,new Color(0, 0.5f, 0.3f, 1.0f));
        this.colors.put( "pink" ,new Color(0.5f, 0, 0.5f, 1.0f));
        this.colors.put( "light-green" ,new Color(0, 0.5f, 0.5f, 1.0f));
        this.colors.put( "yellow" ,new Color(1.0f, 1.0f, 0.0f, 1.0f));
        this.colors.put( "green" ,new Color(0.0f, 1.0f, 0.0f, 1.0f ) );

        return this;
    }

    protected void loadAssets()
    {
        this.joint      =   this.get( "joint.png" , Texture.class );
        this.food       =   this.get( "food.png" , Texture.class );
        this.powerup    =   this.get( "powerup.png" , Texture.class );
        this.wall       =   this.get( "wall.png" , Texture.class );
        this.logo       =   this.get( "icon_lady.png" , Texture.class );

        this.fontRegularFile    =   Gdx.files.internal( "roboto/Roboto-Regular.ttf" );
        this.fontBoldFile       =   Gdx.files.internal( "roboto/Roboto-Bold.ttf" );

        this.fontRegular    =   this.createFont( 20 , fontRegularFile );
        this.fontBold       =   this.createFont( 26 , fontBoldFile );
        this.fontBig        =   this.createFont( 50 , fontBoldFile );
        this.fontSmall      =   this.createFont( 16 , fontRegularFile );

        this.uiskin         =   new Skin( Gdx.files.internal( "skin/uiskin.json" ) );
    }
}
