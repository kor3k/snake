package cz.kor3k.snake.model;

/**
 * Created by martin on 29. 10. 2014.
 */
public class Food extends Block
{
    private int calories    =   1;
    private int score       =   1;

    public Food(float x, float y)
    {
        super(x, y);
    }

    public Food()
    {
        this(0,0);
    }

    public int getCalories() {
        return calories;
    }

    public int getScore() {
        return score;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void eatBy( Snake snake )
    {
        snake.grow( this.getCalories() );
        snake.getPlayer().addScore( this.getScore() );
    }
}
