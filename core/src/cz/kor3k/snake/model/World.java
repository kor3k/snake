package cz.kor3k.snake.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import cz.kor3k.forge.*;
import cz.kor3k.forge.Object;
import cz.kor3k.snake.Settings;
import cz.kor3k.snake.SnakeGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by martin on 29. 10. 2014.
 */
public class World extends cz.kor3k.forge.World
{
    public final List<Snake>    snakes  =   new ArrayList<>();
    public final List<Player>   players =   new ArrayList<>();
    public final List<Food>     foods   =   new ArrayList<>();
    public final List<Powerup>  powerups=   new ArrayList<>();
    public final List<Wall>     walls   =   new ArrayList<>();
    public final List<Wormhole> wormholes   =   new ArrayList<>();

    public transient SnakeGame   game;

    public final int  matrixWidth;
    public final int  matrixHeight;

    private transient Ticker  ticker;
    private transient Ticker.TickerMultiplexer snakeTickers;

    public World( int matrixWidth , int matrixHeight , SnakeGame game )
    {
        this.matrixWidth    =   matrixWidth;
        this.matrixHeight   =   matrixHeight;
        this.game           =   game;
    }

    protected World()
    {
        this( 0 , 0 , null );
    }

    public World create()
    {
        foods.clear();
        powerups.clear();
        walls.clear();
        wormholes.clear();
        snakes.clear();

        int i = 0;
        for( Player player : players )
        {
            i++;

            Snake   snake  =   new Snake( i * 5 , i * 5 , 0 == i % 2 ? Snake.Direction.UP : Snake.Direction.DOWN , 5 );

            player.setSnake(snake);
            snake.setPlayer(player);
            snakes.add( snake );
        }

        this.ticker         =   new Ticker( game.settings.gameSpeed );
        this.snakeTickers   =   new Ticker.TickerMultiplexer();

        for( final Snake snake : this.snakes )
        {
            this.snakeTickers.addTicker(
                    new Ticker( game.settings.maxSnakeSpeed )
                    {
                        public void tick()
                        {
                            if( this.isTickQueued() )
                            {
                                //0.05 - 0.25 , 50 == 0.15

                                float speed = snake.getSpeed();
                                speed   =  ( speed / 100 ) * 20 / 100;
                                speed   =  speed == 0 ? 0 : ( game.settings.minSnakeSpeed - speed );

                                super.tick();
                                if( 0 == speed && snake.new Buffs().hasFreeze() )
                                    return;

                                if( this.getTick() != speed )
                                {
                                    this.setTick( speed );
                                    Gdx.app.debug("World", "speed: " + speed );
                                }

                                snake.advance( matrixWidth , matrixHeight );
                            }
                        }
                    }
            );
        }

        return this;
    }

    public int getMatrixWidth()
    {
        return this.matrixWidth;
    }

    public int getMatrixHeight()
    {
        return this.matrixHeight;
    }

    protected boolean overlapsSnake( cz.kor3k.forge.Object object  )
    {
        for( Snake snake : this.snakes )
        {
            for( Joint joint : snake.getJoints() )
            {
                if( object.overlaps(joint) )
                    return true;
            }
        }

        return false;
    }

    protected boolean overlapsWall( cz.kor3k.forge.Object object  )
    {
        for( Wall wall : this.walls )
        {
            for( Block block : wall.getBlocks() )
            {
                if( object.overlaps(block) )
                    return true;
            }
        }

        return false;
    }

    protected boolean overlapsFood( cz.kor3k.forge.Object object )
    {
        for( Food food : this.foods )
        {
            if( object.overlaps( food ) )
                return true;
        }

        return false;
    }

    protected boolean overlapsPowerup( cz.kor3k.forge.Object object )
    {
        for( Powerup powerup : this.powerups )
        {
            if( object.overlaps( powerup ) )
                return true;
        }

        return false;
    }

    public boolean overlapsAnyObject( cz.kor3k.forge.Object object )
    {
        return  this.overlapsSnake( object )
                || this.overlapsFood( object )
                || this.overlapsPowerup( object )
                || this.overlapsWall( object );
    }

    private void updateTimeouts( float deltaTime )
    {
        if ( !powerups.isEmpty() )
        {
            for (Iterator<Powerup> it = powerups.iterator(); it.hasNext();)
            {
                Powerup p = it.next();

                p.subtractTimeout( deltaTime );
                if( p.isTimeouted() )
                {
                    it.remove();
                }
            }
        }

        for( Snake snake : this.snakes )
        {
            for (Iterator<Snake.Buff> it = snake.getBuffs().iterator(); it.hasNext();)
            {
                Snake.Buff b = it.next();

                b.subtractTimeout( deltaTime );
                if( b.isTimeouted() )
                {
                    b.remove();
                    it.remove();
                }
            }
        }

        if ( !walls.isEmpty() )
        {
            for (Iterator<Wall> it = walls.iterator(); it.hasNext();)
            {
                Wall w = it.next();

                if( -1 == w.getTimeout() ) // -1 == no timeout
                    continue;

                w.subtractTimeout( deltaTime );
                if( w.isTimeouted() )
                {
                    it.remove();
                }
            }
        }

        if ( !wormholes.isEmpty() )
        {
            for (Iterator<Wormhole> it = wormholes.iterator(); it.hasNext();)
            {
                Wormhole w = it.next();

                if( -1 == w.getTimeout() ) // -1 == no timeout
                    continue;

                w.subtractTimeout( deltaTime );
                if( w.isTimeouted() )
                {
                    it.remove();
                }
            }
        }
    }

    private Settings.Player getPlayerSettings( Player player )
    {
        return game.settings.getPlayer( player.getId() );
    }

    private void checkCollisions()
    {
        //check for collision with other snakes and walls
        for( Snake snake : this.snakes )
        {
            Snake.Buffs buffs   =   snake.new Buffs();

            for( Snake other : this.snakes )
            {
                if( other == snake )
                    continue;

                for( Joint joint : other.getJoints() )
                {
                    if( snake.getHead().overlaps( joint ) && !buffs.hasNoclip() && !buffs.hasFreeze() )
                    {
                        if( other.new Buffs().hasFreeze() )
                        {
                            Snake.Buff buff = new Snake.Buff.Freeze( snake );
                            buff.apply( 5 );
                            break;
                        }

                        //snake hits other, loses
                        //but if snake.noclip, nothing happens
                        Gdx.app.debug( "World" , "game over - snake collides with other snake" );
                        Gdx.app.debug( "joint" , Integer.toString( other.getJoints().indexOf( joint ) ) );

                        String playerName       =   this.getPlayerSettings( snake.getPlayer() ).name;
                        String withPlayerName   =   this.getPlayerSettings( other.getPlayer() ).name;
                        Gdx.app.log( "Collision" , "player " + playerName + " collides with PLAYER " + withPlayerName + " at " + snake.getHead().position );

                        Gdx.input.vibrate( 500 );
                        this.setGameState( Game.State.OVER );
                    }
                }
            }

            //check for collision with itself
            for( Joint joint : snake.getJoints() )
            {
                if( joint == snake.getHead() )
                    continue;

                if( snake.getHead().overlaps( joint ) && !buffs.hasNoclip() )
                {
                    //snake hits other, loses
                    //but if snake.noclip, nothing happens
                    Gdx.app.debug( "World" , "game over - snake collides with itself" );

                    String playerName       =   this.getPlayerSettings( snake.getPlayer() ).name;
                    Gdx.app.log( "Collision" , "player " + playerName + " collides with HIMSELF" + " at " + snake.getHead().position );

                    Gdx.input.vibrate( 500 );
                    this.setGameState( Game.State.OVER );
                }
            }

            for( Wall wall : this.walls )
            {
                for( Block block : wall.getBlocks() )
                {
                    if( snake.getHead().overlaps( block ) && !buffs.hasNoclip() )
                    {
                        //snake hits wall, loses
                        Gdx.app.debug( "World" , "game over - snake collides with wall" );

                        String playerName       =   this.getPlayerSettings( snake.getPlayer() ).name;
                        Gdx.app.log( "Collision" , "player " + playerName + " collides with WALL " + " at " + snake.getHead().position );

                        Gdx.input.vibrate( 500 );
                        this.setGameState( Game.State.OVER );
                    }
                }
            }
        }

        //check for collision with food and powerups
        //Iterator is used here because we need Iterator.remove()
        for( Snake snake : this.snakes )
        {
            for (Iterator<Food> it = this.foods.iterator(); it.hasNext();)
            {
                Food food = it.next();

                if( snake.getHead().overlaps( food ) )
                {
                    String playerName       =   this.getPlayerSettings( snake.getPlayer() ).name;
                    Gdx.app.log( "Food" , "player " + playerName + " eats food at " + snake.getHead().position );

                    food.eatBy( snake );
                    it.remove();
                }
            }

            for (Iterator<Powerup> it = this.powerups.iterator(); it.hasNext();)
            {
                Powerup powerup = it.next();

                if( snake.getHead().overlaps( powerup ) )
                {
                    String playerName       =   this.getPlayerSettings( snake.getPlayer() ).name;
                    String powerupType      =   powerup.getClass().toString();
                    powerupType             =   powerupType.substring( powerupType.indexOf( "$" ) + 1 );
                    Gdx.app.log( "Powerup" , "player " + playerName + " eats powerup " + powerupType + " at " + snake.getHead().position );

                    powerup.eatBy( snake );
                    it.remove();
                }
            }
        }

        //check for collision with wormholes
        //if there is a nearest unoccupied position at the other exit, move snake through
        for( Snake snake : this.snakes )
        {
            cz.kor3k.snake.Vector2 warpTo   =   null;

            for( Wormhole wh : this.wormholes )
            {
                if( snake.getHead().overlaps( wh.getExitA() ) )
                {
                    warpTo  =   this.getFirstAvailableNearestRandomPosition( wh.getExitB() );
                }
                else if( snake.getHead().overlaps( wh.getExitB() ) )
                {
                    warpTo  =   this.getFirstAvailableNearestRandomPosition( wh.getExitA() );
                }
            }

            if( null != warpTo )
            {
                String playerName       =   this.getPlayerSettings( snake.getPlayer() ).name;
                Gdx.app.log( "Wormhole" , "player " + playerName + " enters wormhole at " + snake.getHead().position + " and exits at " + warpTo );

                snake.getHead().setPosition( warpTo );
                snake.setDirection( warpTo.getDirection() );
            }
        }
    }

    public void update( float deltaTime )
    {
//        Gdx.app.debug("snake.World", "update()ing width delta: " + Float.toString( deltaTime )  );

        snakeTickers.addDeltaTime( deltaTime );
        ticker.addDeltaTime(deltaTime);

//        Gdx.app.debug( "World" , "tickTime: " + tickTime );
//        Gdx.app.debug( "World" , "deltaTime: " + deltaTime );

//        if( !powerups.isEmpty() )
//        {
//            List<Timeoutable>   to  =   new ArrayList<>(powerups);
//            Timeoutable.subtractTimeout( deltaTime , to );
//            Timeoutable.dropTimeouted( to );
//        }


        this.updateTimeouts( deltaTime );

        if( this.foods.size() < this.snakes.size() )
        {
            this.placeFood();
        }

        int seed    =   (int)( Math.random() * 100 );
//        Gdx.app.debug( "World" , "seed: " + seed );


        if( this.powerups.size() < this.snakes.size() && seed < 5 ) //5% chance
        {
            this.placePowerup();
        }

        if( game.settings.useWalls && this.walls.size() < 1 )
        {
            this.placeWall();
        }

        if( game.settings.useWormholes && this.wormholes.size() < 1 )
        {
            this.placeWormhole();
        }

        while( ticker.isTickQueued() )
        {
//            for( Snake s : snakes )
//            {
//                Gdx.app.debug( "World" , "snake speed: " + s.getSpeed() );
//            }
//
//            for( Ticker t : this.snakeTickers.getTickers() )
//            {
//                Gdx.app.debug( "World" , "ticker time: " + t.getTickTime() );
//                Gdx.app.debug( "World" , "ticker: " + t.getTick() );
//            }

            ticker.tick();

            if( snakeTickers.isTickQueued() )
            {
                snakeTickers.tick();
            }

            //advance snakes
            //for every snake, check collision with other snakes
            //same for food
            //same for powerups
            //same for walls

            this.checkCollisions();

        }


/**
 if (score % 100 == 0 && tick - TICK_DECREMENT > 0) {
 tick -= TICK_DECREMENT;
 }

 **/

    }

    protected Vector2 getRandomPosition()
    {
        Block pos   =   new Block(0,0);
        do
        {
            int randomX =   MathUtils.random( this.getMatrixWidth() - 1 );
            int randomY =   MathUtils.random( this.getMatrixHeight() - 1 );

            pos.setPosition( randomX , randomY );
        }
        while( this.overlapsAnyObject( pos ) );

        return pos.position;
    }

    public Wormhole placeWormhole()
    {
        return this.placeWormhole(5, 10);
    }

    public Wormhole placeWormhole( int minTimeout , int maxTimeout )
    {
        Wormhole    wormhole    =   new Wormhole();

        wormhole.getExitA().setPosition( this.getRandomPosition() );
        wormhole.getExitB().setPosition( this.getRandomPosition() );

        wormhole.setTimeout(MathUtils.random(minTimeout, maxTimeout));
        this.wormholes.add( wormhole );

        Gdx.app.log( "Wormhole" , "placing wormhole at" + wormhole.getExitA().position + " and " + wormhole.getExitB().position );

        return wormhole;
    }

    public Food placeFood()
    {
        Food    food    =   new Food();

        food.setPosition( this.getRandomPosition() );
        this.foods.add( food );

        Gdx.app.log( "Food" , "placing food at" + food.position );

        return food;
    }

    public Wall placeWall()
    {
        return this.placeWall( 5 , 5 , 10 );
    }

    public Wall placeWall( int blocks , int minTimeout , int maxTimeout )
    {
        Wall    wall    =   new Wall();

        wall.getLastBlock().setPosition(this.getRandomPosition());

        Gdx.app.debug("World", "generating wall with x: " + wall.getLastBlock().position.x + " and y: " + wall.getLastBlock().position.y);
        Gdx.app.log( "Wall" , "placing wall at" + wall.getLastBlock().position );


        for( int i = 0 ; i < blocks ; i++ )
        {
            Vector2 pos =   this.getFirstAvailableNearestRandomPosition(wall.getLastBlock());

            if( null == pos )
            {
                //no direction can be used, so exit the for
                break;
            }
            else
            {
                wall.getBlocks().add( new Block( pos.x , pos.y ) );
            }
        }

        wall.setTimeout(MathUtils.random(minTimeout, maxTimeout));
        this.walls.add( wall );
        return wall;
    }

    public Powerup placePowerup( int maxTimeout , int minTimeout )
    {
        Powerup    powerup  =   this.generatePowerup( maxTimeout , minTimeout );

        powerup.setPosition(this.getRandomPosition());
        this.powerups.add( powerup );

        Gdx.app.log( "Powerup" , "placing powerup at" + powerup.position );

        return powerup;
    }

    public  Powerup placePowerup()
    {
        return this.placePowerup( 5 , 8 );
    }

    private int rand()
    {
        return MathUtils.random(0, 100);
    }

    private int rand( int minTime , int maxTime )
    {
        return MathUtils.random( maxTime - minTime ) + minTime;
    }

    private Powerup generatePowerup( int minTime , int maxTime )
    {
        Powerup powerup;
        int     rand    = this.rand();

        if( rand < 5 ) // 5%
        {
            int r = this.rand();
            if( r < 33 )
            {
                powerup =   new Powerup.SwapPlayers( this.rand( minTime , maxTime ) , this.players );
            }
            else if( r < 66 )
            {
                powerup =   new Powerup.SwapSnakes( this.rand( minTime , maxTime ) , this.snakes );
            }
            else
            {
                powerup =   new Powerup.Freeze( this.rand( minTime , maxTime ) );
            }
        }
        else if( rand < 25 ) // 20%
        {
            int r = this.rand();
            if( r < 50 )
            {
                powerup =   new Powerup.Chop( this.rand( minTime , maxTime ) , 5 );
            }
            else
            {
                powerup =   new Powerup.Blockade( this.rand( minTime , maxTime ) , 3 , this );
            }
        }
        else if( rand < 65 ) // 40%
        {
            int r = this.rand();
            if( r < 25 )
            {
                powerup =   new Powerup.Fast( this.rand( minTime , maxTime ) );
            }
            else if( r < 50 )
            {
                powerup =   new Powerup.Slow( this.rand( minTime , maxTime ) );
            }
            else if( r < 75 )
            {
                powerup =   new Powerup.Shrink( this.rand( minTime , maxTime ) );
            }
            else
            {
                powerup =   new Powerup.Reverse( this.rand( minTime , maxTime ) );
            }
        }
        else // 35%
        {
            int r = this.rand();
            if( r < 33 )
            {
                powerup =   new Powerup.Noclip( this.rand( minTime , maxTime ) );
            }
            else if( r < 66 )
            {
                powerup =   new Powerup.Confuse( this.rand( minTime , maxTime ) );
            }
            else
            {
                powerup =   new Powerup.Tunnel( this.rand( minTime , maxTime ) , 3 , this );
            }
        }

        Gdx.app.debug( "Powerup" , "generated powerup of type " + powerup.getClass() );

        return powerup;
    }

    private cz.kor3k.snake.Vector2 getFirstAvailableNearestRandomPosition( Object around )
    {
        List<Integer> directions  =   new ArrayList<>();
        directions.add( Snake.Direction.UP );
        directions.add( Snake.Direction.DOWN );
        directions.add( Snake.Direction.LEFT );
        directions.add( Snake.Direction.RIGHT );

        Collections.shuffle( directions );

        Block block;
        for( int d : directions )
        {
            block   =   new Block( around.position.x , around.position.y );

            if( Snake.Direction.UP == d )
                block.setPosition( block.position.x , block.position.y + 1 );
            else if( Snake.Direction.DOWN == d )
                block.setPosition( block.position.x , block.position.y - 1 );
            else if( Snake.Direction.LEFT == d )
                block.setPosition( block.position.x - 1 , block.position.y );
            else if( Snake.Direction.RIGHT == d )
                block.setPosition( block.position.x + 1 , block.position.y );

            if( block.position.x < 0 || block.position.x >= getMatrixWidth()
                    || block.position.y < 0 || block.position.y >= getMatrixHeight()
                    || this.overlapsAnyObject( block ) )
                continue;
            else
            {
                return new cz.kor3k.snake.Vector2( block.position ).setDirection( d );
            }
        }

        return null;
    }
}
