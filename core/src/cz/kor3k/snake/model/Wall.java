package cz.kor3k.snake.model;

import cz.kor3k.forge.AbstractTimeoutable;
import cz.kor3k.forge.Timeoutable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 29. 10. 2014.
 */
public class Wall extends AbstractTimeoutable implements Timeoutable
{
    private List<Block> blocks  =   new ArrayList<>();
    public  float       timeout =   -1;


    //constructor with position of first block
    //Wormhole
    //JeroMQ
    public Wall( int x , int y , float timeout )
    {
        this( x , y );
        this.timeout    =   timeout;
    }

    public Wall( int x , int y )
    {
        this.blocks.add( new Block( x , y ) );
    }

    public Wall()
    {
        this(0,0);
    }

    public Block getLastBlock()
    {
        return this.blocks.get( this.blocks.size() - 1 );
    }


    public List<Block> getBlocks()
    {
        return this.blocks;
    }

    public Block createBlock( int direction , int matrixWidth , int matrixHeight )
    {
        Block block =   new Block(0,0);
        Block last  =   this.getLastBlock();

        if( Snake.Direction.UP == direction )
        {
            block.setPosition( last.position.x , last.position.y - 1 );
        }
        else if( Snake.Direction.LEFT == direction )
        {
            block.setPosition( last.position.x - 1 , last.position.y );
        }
        else if( Snake.Direction.DOWN == direction )
        {
            block.setPosition( last.position.x , last.position.y + 1 );
        }
        else if( Snake.Direction.RIGHT == direction )
        {
            block.setPosition( last.position.x + 1 , last.position.y );
        }
        else
        {
            throw new RuntimeException("You must set a valid Snake.Direction : 0-3");
        }

        if(
                block.position.x >= matrixWidth
                || block.position.y >= matrixHeight
                || block.position.x < 0
                || block.position.y < 0
                )
        {
            return null;
        }

        return block;
    }


    @Override
    public float getTimeout()
    {
        return timeout;
    }

    @Override
    public void setTimeout(float timeout)
    {
        this.timeout    =   timeout;
    }
}
