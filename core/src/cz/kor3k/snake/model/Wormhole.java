package cz.kor3k.snake.model;

import cz.kor3k.forge.AbstractTimeoutable;
import cz.kor3k.forge.Object;
import cz.kor3k.forge.Timeoutable;

/**
 * Created by martin on 5. 11. 2014.
 */
public class Wormhole extends AbstractTimeoutable implements Timeoutable
{
    private Object   exitA;
    private Object   exitB;
    public  float    timeout    =   -1;

    public Wormhole( int aX , int aY , int bX , int bY )
    {
        this.exitA  =   new Object( aX , aY , 1 , 1 );
        this.exitB  =   new Object( bX , bY , 1 , 1 );
    }

    public Wormhole()
    {
        this( 0 , 0 , 0 , 0 );
    }

    public Wormhole( float timeout )
    {
        this();
        this.setTimeout( timeout );
    }

    public Object getExitA() {
        return exitA;
    }

    public Object getExitB() {
        return exitB;
    }

    @Override
    public float getTimeout() {
        return this.timeout;
    }

    @Override
    public void setTimeout(float timeout) {
        this.timeout    =   timeout;
    }
}
