package cz.kor3k.snake.model;


/**
 * Created by martin on 29. 10. 2014.
 */
public class Joint extends Block
{
    public Joint(float x, float y )
    {
        super(x, y);
    }

    protected Joint()
    {
        super();
    }
}
