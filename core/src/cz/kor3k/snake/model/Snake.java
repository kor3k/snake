package cz.kor3k.snake.model;

import cz.kor3k.forge.AbstractTimeoutable;
import cz.kor3k.forge.Timeoutable;
import cz.kor3k.snake.controller.PlayerController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by martin on 29. 10. 2014.
 */
public class Snake
{
    private int         direction;
    private int         speed   =   50; //anebo extends forge.DynamicObject ?
    private List<Joint> joints  =   new ArrayList<>();
    private transient Player      player;
    private int         grow;
    private List<Buff>  buffs   =   new ArrayList<>();

    public static final class Direction
    {
        public static final int UP = 0 , LEFT = 1 , DOWN = 2 , RIGHT = 3;
    }

    public Snake( int x , int y , int direction , int startLength )
    {
        //place head
        this.joints.add( new Joint( x , y ) );
        this.direction  =   direction;
        this.grow( startLength );
    }

    protected Snake()
    {

    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<Joint> getJoints()
    {
        return joints;
    }

    public void setDirection( int direction )
    {
        if( !( direction >= 0 && direction <= 3 ) )
            return;

        this.direction  =   direction;
    }

    public void turnLeft()
    {
        direction += 1;
        if (direction > Direction.RIGHT)
            direction = Direction.UP;
    }

    public void turnRight()
    {
        direction -= 1;
        if (direction < Direction.UP)
            direction = Direction.RIGHT;
    }

    public void reverse()
    {
        if( Direction.UP == direction )
        {
            direction = Direction.DOWN;
        }
        else if( Direction.DOWN == direction )
        {
            direction = Direction.UP;
        }
        else if( Direction.LEFT == direction )
        {
            direction = Direction.RIGHT;
        }
        else if( Direction.RIGHT == direction )
        {
            direction = Direction.LEFT;
        }

        Collections.reverse( this.joints );
    }

    public void grow( int grow )
    {
        this.grow   +=  grow;
    }

    public Joint getHead()
    {
        return this.joints.get( 0 );
    }

    public Joint getTail()
    {
        return this.joints.get( this.joints.size() - 1 );
    }

    public void advance( int matrixWidth , int matrixHeight )
    {
        Joint   head    =   this.getHead();
        Joint   add     =   new Joint( head.position.x , head.position.y );

        //compute coordinates
        float x = head.position.x;
        float y = head.position.y;

        if( direction == Direction.UP )
        {
            y++;
        }
        else if( direction == Direction.LEFT )
        {
            x--;
        }
        else if( direction == Direction.DOWN )
        {
            y--;
        }
        else if( direction == Direction.RIGHT )
        {
            x++;
        }

        if( x < 0 )
        {
            x   =   matrixWidth - 1;
        }
        else if( x > ( matrixWidth - 1 ) )
        {
            x   =   0;
        }
        if( y < 0 )
        {
            y   =   matrixHeight - 1;
        }
        else if( y > ( matrixHeight - 1 ) )
        {
            y   =   0;
        }

        //move head
        head.setPosition( x , y );
        //add new joint after head
        this.joints.add( 1 , add );

        if( this.grow > 0 )
        {
            --this.grow;
        }
        else
        {
            this.joints.remove( this.getTail() );
        }
    }

    public int getSpeed()
    {
        return this.speed;
    }

    public void setSpeed( int speed )
    {
        if( speed < 0 )
        {
            this.speed  =   0;
        }
        else if( speed  > 100 )
        {
            this.speed  =   100;
        }
        else
        {
            this.speed  =   speed;
        }
    }

    public List<Buff> getBuffs()
    {
        return this.buffs;
    }

    public class Buffs
    {
        public Buff.Noclip getNoclip()
        {
            for( Buff buff : buffs )
            {
                if( buff instanceof Buff.Noclip )
                {
                    return (Buff.Noclip)buff;
                }
            }

            return null;
        }

        public Buff.Confuse getConfuse()
        {
            for( Buff buff : buffs )
            {
                if( buff instanceof Buff.Confuse )
                {
                    return (Buff.Confuse)buff;
                }
            }

            return null;
        }

        public Buff.Speed getSpeed()
        {
            for( Buff buff : buffs )
            {
                if( buff instanceof Buff.Speed )
                {
                    return (Buff.Speed)buff;
                }
            }

            return null;
        }

        public Buff.Freeze getFreeze()
        {
            for( Buff buff : buffs )
            {
                if( buff instanceof Buff.Freeze )
                {
                    return (Buff.Freeze)buff;
                }
            }

            return null;
        }

        public boolean hasNoclip()
        {
            if( null == this.getNoclip() )
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public boolean hasSpeed()
        {
            if( null == this.getSpeed() )
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public boolean hasConfuse()
        {
            if( null == this.getConfuse() )
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public boolean hasFreeze()
        {
            if( null == this.getFreeze() )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public static abstract class Buff extends AbstractTimeoutable implements Timeoutable
    {
        private float timeout;
        protected transient Snake snake;

        public Buff( Snake snake )
        {
            this.setSnake( snake );
        }

        protected Buff()
        {

        }

        public void setSnake( Snake snake )
        {
            this.snake  =   snake;
        }


        public abstract void apply();
        public abstract void remove();

        public void apply( int timeout )
        {
            this.timeout    =   timeout;
            this.snake.getBuffs().add( this );
            this.apply();
        }

        @Override
        public float getTimeout()
        {
            return this.timeout;
        }

        @Override
        public void setTimeout(float timeout)
        {
            this.timeout    =   timeout;
        }

        public static class Noclip extends Buff
        {
            public Noclip(Snake snake) {
                super(snake);
            }

            protected Noclip()
            {

            }

            @Override
            public void apply() {

            }

            @Override
            public void remove() {

            }
        }

        public static class Confuse extends Buff
        {
            public Confuse(Snake snake )
            {
                super(snake);
            }

            protected Confuse()
            {

            }

            @Override
            public void apply()
            {

            }

            @Override
            public void remove()
            {

            }
        }

        public static class Freeze extends Speed
        {
            public Freeze(Snake snake) {
                super(snake,0);
            }

            protected Freeze()
            {

            }

            public void apply()
            {
                //freezes snake - sets speed to 0
                //if another snakes hits frozen snake, it becomes frozen too, ie it gets Buff.Freeze

                super.apply();
                snake.setSpeed( 0 );
            }
        }

        public static class Speed extends Buff
        {
            private int originalSpeed;
            private int amount;

            public Speed(Snake snake, int amount)
            {
                super(snake);
                this.amount =   amount;
            }

            protected Speed()
            {

            }

            public void apply()
            {
                this.originalSpeed  =   snake.getSpeed();
                snake.setSpeed( this.originalSpeed + this.amount );
            }

            public void remove()
            {
                snake.setSpeed( this.originalSpeed );
            }
        }
    }
}
