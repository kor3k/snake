package cz.kor3k.snake.model;

import com.badlogic.gdx.Gdx;

/**
 * Created by martin on 29. 10. 2014.
 */
public class Player
{
    private int     score   =   0;
    private Snake   snake;
    private String  id;

    public Player()
    {

    }

    public Player( String id )
    {
        this.setId( id );
    }

    public Player setId( String id )
    {
        this.id =   id;
        return this;
    }

    public String getId()
    {
        return this.id;
    }

    @Override
    public int hashCode()
    {
        return 0;
    }

    @Override
    public boolean equals(Object obj)
    {
        if( obj instanceof Player )
        {
            if( ((Player) obj).getId().equals( this.getId() )  )
            {
                return true;
            }
            else
            {
                if( null == ((Player) obj).getId() )
                {
                    Gdx.app.log( "model.Player.equals" , "compared Player's id is not set" );
                }

                if( null == this.id )
                {
                    Gdx.app.log( "model.Player.equals" , "this Player's id is not set" );
                }
            }

        }

        return false;
    }

    public int addScore( int score )
    {
        return this.score += score;
    }

    public int subScore( int score )
    {
        return this.score -= score;
    }

    public int getScore()
    {
        return this.score;
    }

    public Snake getSnake() {
        return this.snake;
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
    }
}
