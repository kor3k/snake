package cz.kor3k.snake.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import cz.kor3k.forge.Timeoutable;
import cz.kor3k.snake.controller.PlayerController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by martin on 29. 10. 2014.
 */
public abstract class Powerup extends Food implements Timeoutable
{
    private float timeout;

    public Powerup(float x, float y, int timeout)
    {
        super(x, y);
        this.timeout    =   timeout;
        this.setScore( 2 );
    }

    public void subtractTimeout( float time )
    {
        this.setTimeout( this.getTimeout() - time );
    }

    public boolean isTimeouted()
    {
        return this.getTimeout() <= 0;
    }

    @Override
    public float getTimeout()
    {
        return this.timeout;
    }

    @Override
    public void setTimeout(float timeout)
    {
        this.timeout    =   timeout;
    }

    public Powerup( int timeout )
    {
        this( 0 , 0 , timeout );
        Gdx.app.debug( "Powerup" , "instatiating powerup " + this.getClass() );
    }

    protected Powerup()
    {

    }

    public void eatBy( Snake snake )
    {
        Gdx.app.debug("Powerup", "powerup " + this.getClass() + " gets eaten by snake " + snake.toString() );
        super.eatBy( snake );
    }

    public static String getInfo()
    {
        return "";
    }

    //iterate over hashmap
//    public static void printMap(Map mp)
//    {
//        Iterator it = mp.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry pairs = (Map.Entry)it.next();
//            System.out.println(pairs.getKey() + " = " + pairs.getValue());
//            it.remove(); // avoids a ConcurrentModificationException
//        }

//    Map<Integer, Integer> map = new HashMap<Integer, Integer>();
//    for (Map.Entry<Integer, Integer> entry : map.entrySet())
//    {
//        System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
//    }

//    }

    public static class Fast extends Powerup
    {
        public Fast(int timeout) {
            super(timeout);
        }

        protected Fast()
        {

        }

        public void eatBy( Snake snake )
        {
            Snake.Buff buff = new Snake.Buff.Speed( snake , +20 );
            buff.apply( 10 );

            super.eatBy( snake );
        }

        public static String getInfo()
        {
            return "speed snake up";
        }
    }

    public static class Slow extends Powerup
    {
        public Slow(int timeout) {
            super(timeout);
        }

        protected Slow()
        {

        }

        public void eatBy( Snake snake )
        {
            Snake.Buff buff = new Snake.Buff.Speed( snake , -20 );
            buff.apply( 10 );

            super.eatBy( snake );
        }

        public static String getInfo()
        {
            return "slow snake down";
        }
    }

    public static class Freeze extends Powerup
    {
        public Freeze(int timeout) {
            super(timeout);
        }

        protected Freeze()
        {

        }

        public void eatBy( Snake snake )
        {
            Snake.Buff buff = new Snake.Buff.Freeze( snake );
            buff.apply( 5 );

            super.eatBy( snake );
        }

        public static String getInfo()
        {
            return "freeze snake; every snake that collides with it becomes frozen too";
        }
    }


    public static class Shrink extends Powerup
    {
        public Shrink(int timeout) {
            super(timeout);
        }

        //ShrinkAll

        protected Shrink()
        {

        }

        public void eatBy( Snake snake )
        {
            this.shrink( snake );
            super.eatBy( snake );
        }

        private void shrink( Snake snake )
        {
            int joints  =   snake.getJoints().size()-1;
            Joint head  =   snake.getHead();

            snake.getJoints().clear();
            snake.getJoints().add( head );
            snake.grow( joints );
        }

        public static String getInfo()
        {
            return "remove all snake's joints but head and make it grow for their count";
        }
    }

    public static class Reverse extends Powerup
    {
        public Reverse(int timeout) {
            super(timeout);
        }

        protected Reverse()
        {

        }

        public void eatBy( Snake snake )
        {
            snake.reverse();
        }

        public static String getInfo()
        {
            return "swap snakes' head and tail and reverse direction";
        }
    }

    public static class Chop extends Powerup
    {
        private int chopJoints;

        public Chop(int timeout, int chopJoints )
        {
            super(timeout);
            this.chopJoints =   chopJoints;
        }

        protected Chop()
        {

        }

        public void eatBy( Snake snake )
        {
            for( int i = 0 ; i < this.chopJoints ; i++ )
            {
                if( snake.getJoints().size() <= 2 )
                    break;

                snake.getJoints().remove( snake.getJoints().size() - 1 );
            }

            super.eatBy( snake );
        }

        public static String getInfo()
        {
            return "cut off X tail joints";
        }
    }

    public static class SwapSnakes extends Powerup
    {
        private List<Snake> snakes;

        public SwapSnakes(int timeout , List<Snake> snakes )
        {
            super(timeout);
            this.snakes =   snakes;
        }

        protected SwapSnakes()
        {

        }

        void shrink( Snake snake )
        {
            int joints  =   snake.getJoints().size()-1;
            Joint head  =   snake.getHead();

            snake.getJoints().clear();
            snake.getJoints().add( head );
            snake.grow( joints );
        }

        public void eatBy( Snake snake )
        {
            super.eatBy( snake );

            List<Vector2> positions =   new ArrayList<>();
            for( Snake s : this.snakes )
            {
                positions.add( s.getHead().position.cpy() );
            }

            //if only 2 players, swap them instead shuffle
            if( 2 == this.snakes.size() )
            {
                Snake   s0  =   this.snakes.get(0);
                Snake   s1  =   this.snakes.get(1);

                this.shrink( s0 );
                this.shrink( s1 );
                s0.getHead().setPosition( positions.get(1) );
                s1.getHead().setPosition( positions.get(0) );
            }
            else
            {
                Collections.shuffle( positions );

                int i = 0;
                for( Snake s : this.snakes )
                {
                    this.shrink( s );
                    s.getHead().setPosition( positions.get(i) );

                    i++;
                }
            }
        }

        public static String getInfo()
        {
            return "shrink snakes and swap their position";
        }
    }

    public static class SwapPlayers extends Powerup
    {
        private List<Player>    players;

        public SwapPlayers(int timeout , List<Player> players )
        {
            super(timeout);
            this.players    =   players;
        }

        protected SwapPlayers()
        {

        }

        public void eatBy( Snake snake )
        {
            super.eatBy( snake );

            List<Snake> snakes  =   new ArrayList<>();

            for( Player p : this.players )
            {
                snakes.add( p.getSnake() );
            }

            //if only 2 players, swap them instead shuffle
            if( 2 == this.players.size() )
            {
                Player  p0  =   this.players.get(0);
                Player  p1  =   this.players.get(1);
                Snake   s0  =   p0.getSnake();
                Snake   s1  =   p1.getSnake();

                p0.setSnake( s1 );
                p1.setSnake( s0 );
                s0.setPlayer( p1 );
                s1.setPlayer( p0 );
            }
            else
            {
                Collections.shuffle( snakes );

                int i = 0;
                for( Player p : this.players )
                {
                    if( !snakes.get(i).equals( p.getSnake() ) )
                    {
                        p.setSnake( snakes.get(i) );
                        snakes.get(i).setPlayer(p);
                    }

                    i++;
                }
            }
        }

        public static String getInfo()
        {
            return "swap players' snakes";
        }
    }

    public static class Noclip extends Powerup
    {
        public Noclip(int timeout) {
            super(timeout);
        }

        protected Noclip()
        {

        }

        public void eatBy( Snake snake )
        {
            Snake.Buffs sb  =   snake.new Buffs();

            if( !sb.hasNoclip() )
            {
                Snake.Buff buff = new Snake.Buff.Noclip( snake );
                buff.apply( 10 );
            }
            else
            {
                sb.getNoclip().setTimeout( sb.getNoclip().getTimeout() + 10 );
            }

            super.eatBy( snake );
        }

        public static String getInfo()
        {
            return "snake can pass through snakes and walls";
        }
    }

    public static class Confuse extends Powerup
    {
        public Confuse(int timeout )
        {
            super(timeout);
        }

        protected Confuse()
        {

        }

        public void eatBy( Snake snake )
        {
            Snake.Buffs sb  =   snake.new Buffs();

            if( !sb.hasConfuse() )
            {
                Snake.Buff buff = new Snake.Buff.Confuse( snake );
                buff.apply( 10 );
            }
            else
            {
                sb.getConfuse().setTimeout( sb.getConfuse().getTimeout() + 10 );
            }

            super.eatBy( snake );
        }

        public static String getInfo()
        {
            return "swap players' controls";
        }
    }

    public static class Blockade extends Powerup
    {
        private int     walls;
        private transient World   world;

        public Blockade( int timeout , int walls , World world )
        {
            super(timeout);
            this.walls  =   walls;
            this.world  =   world;
        }

        protected Blockade()
        {

        }

        public void setWorld( World world )
        {
            this.world  =   world;
        }

        public void eatBy(Snake snake)
        {
            for( int i = 0 ; i < walls ; i++ )
            {
                this.world.placeWall();
            }

            super.eatBy(snake);
        }

        public static String getInfo()
        {
            return "place X walls with timeout";
        }
    }

    public static class Tunnel extends Powerup
    {
        private int     wormholes;
        private transient World   world;

        public Tunnel( int timeout , int wormholes , World world )
        {
            super(timeout);
            this.wormholes  =   wormholes;
            this.world      =   world;
        }

        protected Tunnel()
        {

        }

        public void setWorld( World world )
        {
            this.world  =   world;
        }

        public void eatBy(Snake snake)
        {
            for( int i = 0 ; i < wormholes ; i++ )
            {
                this.world.placeWormhole();
            }

            super.eatBy(snake);
        }

        public static String getInfo()
        {
            return "place X wormholes";
        }
    }
}
