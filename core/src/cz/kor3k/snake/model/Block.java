package cz.kor3k.snake.model;


/**
 * Created by martin on 29. 10. 2014.
 */
public class Block extends cz.kor3k.forge.Object
{
    public Block(float x, float y)
    {
        super(x, y, 1, 1);
    }

    protected Block()
    {
        super();
    }
}
