package cz.kor3k.snake.android;

import android.os.Build;
import android.os.Bundle;

import android.view.View;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import cz.kor3k.forge.Hostname;
import cz.kor3k.snake.SnakeGame;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		config.useAccelerometer	=	false;
		config.useCompass		=	false;
		config.useWakelock		=	true;
//		config.numSamples		=	2;
		config.useImmersiveMode	=	true;
		config.hideStatusBar	=	true;

		initialize(new SnakeGame( new Hostname.AndroidHostname() ), config);
	}

	private void setImmersiveFullscreen()
	{
		int uiOptions 		= 	getWindow().getDecorView().getSystemUiVisibility();
		int newUiOptions 	= 	uiOptions;

		if (Build.VERSION.SDK_INT >= 11) {
			newUiOptions ^= View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
			newUiOptions ^= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
			newUiOptions ^= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
		}

		// Navigation bar hiding:  Backwards compatible to ICS.
		if (Build.VERSION.SDK_INT >= 14) {
			newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
		}

		// Status bar hiding: Backwards compatible to Jellybean
		if (Build.VERSION.SDK_INT >= 16) {
			newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
		}

		if (Build.VERSION.SDK_INT >= 18) {
			newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
		}

		getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);

//		if( hasFocus )
//			this.setImmersiveFullscreen();
	}
}
